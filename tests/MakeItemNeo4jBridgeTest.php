<?php
/**
 *  
 *
 * @version 1.0
 * Date: 12/26/13
 * Time: 10:54 AM
 */

require_once( '__init__.php' );

use Everyman\Neo4j\Client,
    Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Batch,
    Everyman\Neo4j\Relationship,
    Everyman\Neo4j\Node,
    Everyman\Neo4j\Traversal,
    Everyman\Neo4j\Cypher;

ini_set('memory_limit','2000M');

class MakeItemNeo4jBridgeTest extends PHPUnit_Framework_TestCase {

    private $bridge;
    public $neo4jConn;
    private $categories = array(
        1 => 'xayaza',
        2 => 'xbybzb',
        3 => 'xcyczc',
        4 => 'xdydzd',
        5 => 'xeyeze',
        6 => 'xfyfzf',
        7 => 'xgygzg',
        8 => 'xhyhzh',
        9 => 'xiyizi',
        10 => 'xjyjzj1',
        11 => 'xjyjzj2',
        12 => 'xjyjzj3',
        13 => 'xjyjzj4',
        14 => 'xjyjzj5',
        15 => 'xjyjzj6',
        16 => 'xjyjzj7',
        17 => 'xjyjzj8',
        18 => 'xjyjzj9',
        19 => 'xjyjzj10',
        20 => 'xjyjzj11',
        21 => 'xjyjzj12',
        22 => 'xjyjzj13',
        23 => 'xjyjzj14',
        24 => 'xjyjzj15',
        25 => 'xjyjzj16',
        26 => 'xjyjzj17',
        27 => 'xjyjzj18',
        28 => 'xjyjzj19',
        29 => 'xjyjzj20',
        30 => 'xjyjzj21',
    );
    /**
     * @dataProvider provider
     */
    public function testMakeItem( $nodeId, $data ){

        $json = $this->bridge->makeItem( $nodeId, $data );
        file_put_contents( 'testMakeItem.log', $json."\n", FILE_APPEND );
        $respond = json_decode( $json, true );
        if( $respond['result'] == 'failure' ){
            echo $respond['errorMessage'];
        }
        $this->assertEquals( $respond['result'], 'success' );
    }

    /**
     * 1000 nodes update data item
     * @return array
     */
    public function provider()
    {
        $this->neo4jConn = new Everyman\Neo4j\Client();
        $queryString = "START n = node:Nodes('id:*') " .
            "RETURN n.id ".
            "LIMIT 1000";

        $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
        $res = $query->getResultSet();

        $data = array();
        $nodeIds= array();
        if( !empty( $res ) ) {
            foreach( $res as $row ){
                $nodeIds[] = $row['n.id'];
            }

            if( !empty( $nodeIds ) ){
                for( $i=1; $i<=1000; $i++ ){
                    $data[] = array(
                        $nodeIds[array_rand( $nodeIds )],
                        $this->_makeItemData()
                    );
                }
            }
        }
        else{
            echo "No nodes are found\n";
        }

        return $data;
    }

    private function _makeItemData( ){

        $category = "{$this->categories[ rand(1,30) ]}.{$this->categories[ rand(1,30) ]}.{$this->categories[ rand(1,30) ]}";

        $itemData = array(
            'id' => rand( 1, 1000000 ),
            "link_id" => rand( 10, 100 ),
            "owner_comment" => uniqid( "Comment:"),
            "created_time" => rand( 0, 1354233600 ),
            "url" => uniqid( "Url:"),
            "email" => uniqid( "Email:"),
            "title" => uniqid( "Title:"),
            "category" => $category,
            "img" => uniqid( "img:"),
            "rate" => uniqid( "rate:"),
            "reference" => uniqid( "reference:"),
            "city" => uniqid( "city:")
        );

        return $itemData;
    }

    public function setUp( ){
        $this->bridge = new Neo4jBridge();
    }
}
 