<?php
/**
 *  
 *
 * @version 1.0
 * Date: 12/26/13
 * Time: 10:54 AM
 */

require_once( '__init__.php' );

use Everyman\Neo4j\Client,
    Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Batch,
    Everyman\Neo4j\Relationship,
    Everyman\Neo4j\Node,
    Everyman\Neo4j\Traversal,
    Everyman\Neo4j\Cypher;

ini_set('memory_limit','2000M');

class QueryItemsBridgeTest extends PHPUnit_Framework_TestCase {

    private $bridge;
    public $neo4jConn;

    /**
     * @dataProvider provider
     */
    public function testQueryItems( $nodeId, $cat1, $cat2, $cat3 ){
        $json = $this->bridge->queryItems( $nodeId, $cat1, $cat2, $cat3 );
        $respond = json_decode( $json, true );
        file_put_contents( 'testQueryItems.log', $json."\n", FILE_APPEND );
        if( $respond['result'] == 'failure' ){
            echo $respond['errorMessage'];
        }
        $this->assertEquals( $respond['result'], 'success' );
    }

    /**
     * Read 1000 data items
     * @return array
     */
    public function provider()
    {
        $this->neo4jConn = new Everyman\Neo4j\Client();
        $queryString = "START item = node:Items('id:*')
                        MATCH owner-[:DATA]->item
                        RETURN owner.id, item.category
                        LIMIT 1000";

        $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
        $res = $query->getResultSet();

        $data = array();

        if( !empty( $res ) ) {
            foreach( $res as $row ){
                $catData = explode( ".", $row['item.category'] );
                $cat1 = $catData[0];
                if( rand( 0, 100 ) > 33 ){
                    $cat2 = $catData[1];
                }
                else{
                    $cat2 = null;
                }
                if( ( rand( 0, 100 ) > 50 ) && !empty( $cat2 ) ){
                    $cat3 = $catData[2];
                }
                else{
                    $cat3 = null;
                }

                $data[] = array(
                    $row['owner.id'],
                    $cat1,
                    $cat2,
                    $cat3
                );
            }
        }
        else{
            echo "No items are found\n";
        }

        return $data;
    }

    public function setUp( ){
        $this->bridge = new Neo4jBridge();
    }
}
 