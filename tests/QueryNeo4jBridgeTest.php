<?php
/**
 *  
 *
 * @version 1.0
 * Date: 12/26/13
 * Time: 10:54 AM
 */

require_once( '__init__.php' );

use Everyman\Neo4j\Client,
    Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Batch,
    Everyman\Neo4j\Relationship,
    Everyman\Neo4j\Node,
    Everyman\Neo4j\Traversal,
    Everyman\Neo4j\Cypher;

ini_set('memory_limit','2000M');

class QueryBridgeTest extends PHPUnit_Framework_TestCase {

    private $bridge;
    public $neo4jConn;

    /**
     * @dataProvider provider
     */
    public function testQuery( $startNodeId, $cat1, $cat2, $cat3 ){

        $json = $this->bridge->query( $startNodeId, $cat1, $cat2, $cat3 );
        file_put_contents( 'testQuery.log', $json."\n", FILE_APPEND );
        $respond = json_decode( $json, true );
        if( $respond['result'] == 'failure' ){
            echo $respond['errorMessage'];
        }

        $this->assertEquals( $respond['result'], 'success' );
    }

    /**
     * Prepare 1000 test data
     * @return array
     */
    public function provider()
    {
        return $this->_getLongPaths() + $this->_getShortPaths() + $this->_getAnyPaths();
    }

    private function _getAnyPaths( ){
        $this->neo4jConn = new Everyman\Neo4j\Client();
        $queryString = "START nodeA=node:Nodes('id:*'),
                            item=node:ItemCat1('category:*')
                            MATCH nodeA-->item
                            RETURN nodeA.id, item.category
                            LIMIT 400";

        $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
        $res = $query->getResultSet();

        $data = array();

        if( !empty( $res ) ) {
            foreach( $res as $row ){
                $catData = explode( ".", $row['item.category'] );
                $cat1 = $catData[0];
                if( rand( 0, 100 ) > 33 ){
                    $cat2 = $catData[1];
                }
                else{
                    $cat2 = null;
                }
                if( ( rand( 0, 100 ) > 50 ) && !empty( $cat2 ) ){
                    $cat3 = $catData[2];
                }
                else{
                    $cat3 = null;
                }

                $data[] = array(
                    $row['nodeA.id'],
                    $cat1,
                    $cat2,
                    $cat3
                );
            }
        }
        return $data;
    }

    private function _getShortPaths( ){
        $this->neo4jConn = new Everyman\Neo4j\Client();
        $queryString = "START nodeA=node:Nodes('id:*'),
                            friend2=node:Cat1('category:*'),
                            item=node:ItemCat1('category:*')
                            MATCH nodeA-[:FRIEND]-friend2-[:DATA]->item
                            RETURN nodeA.id, item.category
                            LIMIT 300";

        $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
        $res = $query->getResultSet();

        $data = array();

        if( !empty( $res ) ) {
            foreach( $res as $row ){
                $catData = explode( ".", $row['item.category'] );
                $cat1 = $catData[0];
                if( rand( 0, 100 ) > 33 ){
                    $cat2 = $catData[1];
                }
                else{
                    $cat2 = null;
                }
                if( ( rand( 0, 100 ) > 50 ) && !empty( $cat2 ) ){
                    $cat3 = $catData[2];
                }
                else{
                    $cat3 = null;
                }

                $data[] = array(
                    $row['nodeA.id'],
                    $cat1,
                    $cat2,
                    $cat3
                );
            }

        }
        return $data;
    }

    private function _getLongPaths( ){
        $this->neo4jConn = new Everyman\Neo4j\Client();
        $queryString = "START nodeA=node:Nodes('id:*'),
                            friend2=node:Cat1('category:*'),
                            item=node:ItemCat1('category:*')
                            MATCH nodeA-[:FRIEND]-friend1-[:FRIEND]-friend2-[:DATA]->item
                            RETURN nodeA.id, item.category
                            LIMIT 300";

        $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
        $res = $query->getResultSet();

        $data = array();

        if( !empty( $res ) ) {
            foreach( $res as $row ){
                $catData = explode( ".", $row['item.category'] );
                $cat1 = $catData[0];
                if( rand( 0, 100 ) > 33 ){
                    $cat2 = $catData[1];
                }
                else{
                    $cat2 = null;
                }
                if( ( rand( 0, 100 ) > 50 ) && !empty( $cat2 ) ){
                    $cat3 = $catData[2];
                }
                else{
                    $cat3 = null;
                }

                $data[] = array(
                    $row['nodeA.id'],
                    $cat1,
                    $cat2,
                    $cat3
                );
            }

        }
        return $data;
    }

    public function setUp( ){
        $this->bridge = new Neo4jBridge();
    }
}
 