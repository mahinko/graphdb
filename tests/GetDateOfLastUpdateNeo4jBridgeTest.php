<?php
/**
 *  
 *
 * @version 1.0
 * Date: 12/26/13
 * Time: 10:54 AM
 */

require_once( '__init__.php' );

use Everyman\Neo4j\Client,
    Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Batch,
    Everyman\Neo4j\Relationship,
    Everyman\Neo4j\Node,
    Everyman\Neo4j\Traversal,
    Everyman\Neo4j\Cypher;

ini_set('memory_limit','2000M');

class GetDateOfLastUpdateNeo4jBridgeTest extends PHPUnit_Framework_TestCase {

    private $bridge;
    public $neo4jConn;

    /**
     * @dataProvider provider
     */
    public function testGetDateOfLastUpdateNeo4j( $nodeId ){
        $json = $this->bridge->getDateOfLastUpdate( $nodeId );
        $respond = json_decode( $json, true );
        file_put_contents( 'testGetDateOfLastUpdateNeo4j.log', $json."\n", FILE_APPEND );
        if( $respond['result'] == 'failure' ){
            echo $respond['errorMessage'];
        }
        $this->assertEquals( $respond['result'], 'success' );
    }

    /**
     * Read 1000 data items
     * @return array
     */
    public function provider()
    {
        $this->neo4jConn = new Everyman\Neo4j\Client();
        $queryString = "START owner = node:Nodes('id:*')
                        RETURN owner.id
                        LIMIT 1000";

        $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
        $res = $query->getResultSet();
        $data = array();
        if( !empty( $res ) ) {
            foreach( $res as $row ){
                $data[] = array( $row['owner.id'] );
            }
        }
        else{
            echo "No Nodes are found\n";
        }
        return $data;
    }

    public function setUp( ){
        $this->bridge = new Neo4jBridge();
    }
}
 