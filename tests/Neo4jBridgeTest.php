<?php
/**
 *  
 *
 * @version 1.0
 * Date: 12/26/13
 * Time: 10:54 AM
 */

require_once( '__init__.php' );

use Everyman\Neo4j\Client,
    Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Batch,
    Everyman\Neo4j\Relationship,
    Everyman\Neo4j\Node,
    Everyman\Neo4j\Traversal,
    Everyman\Neo4j\Cypher;

ini_set('memory_limit','2000M');

class Neo4jBridgeTest extends PHPUnit_Framework_TestCase {

    private $bridge;
    public $neo4jConn;
    private $categories = array(
        1 => 'xayaza',
        2 => 'xbybzb',
        3 => 'xcyczc',
        4 => 'xdydzd',
        5 => 'xeyeze',
        6 => 'xfyfzf',
        7 => 'xgygzg',
        8 => 'xhyhzh',
        9 => 'xiyizi',
        10 => 'xjyjzj1',
        11 => 'xjyjzj2',
        12 => 'xjyjzj3',
        13 => 'xjyjzj4',
        14 => 'xjyjzj5',
        15 => 'xjyjzj6',
        16 => 'xjyjzj7',
        17 => 'xjyjzj8',
        18 => 'xjyjzj9',
        19 => 'xjyjzj10',
        20 => 'xjyjzj11',
        21 => 'xjyjzj12',
        22 => 'xjyjzj13',
        23 => 'xjyjzj14',
        24 => 'xjyjzj15',
        25 => 'xjyjzj16',
        26 => 'xjyjzj17',
        27 => 'xjyjzj18',
        28 => 'xjyjzj19',
        29 => 'xjyjzj20',
        30 => 'xjyjzj21',
    );

    /**
     * @dataProvider provider
     */
    public function testCreateSelectPath( $testData ){

        $this->bridge->updateFriends( $testData['a'], array( $testData['c'], $testData['d'], $testData['e'] ) );
        $this->bridge->updateFriends( $testData['f'], array( $testData['c'], $testData['d'] ) );

        $this->bridge->makeItem( $testData['f'], $testData['x'] );
        $this->bridge->makeItem( $testData['c'], $testData['y'] );

        // Short path check
        $yCatData = explode( ".", $testData['y']['category'] );
        $yPathsJSON = $this->bridge->query( $testData['a'], $yCatData[0], $yCatData[1], $yCatData[2] );
        $yRespond = json_decode( $yPathsJSON, true );
        $this->assertEquals( $yRespond['paths'][0][0], $testData['a'] );
        $this->assertTrue( $yRespond['paths'][0][1] === null );
        $this->assertEquals( $yRespond['paths'][0][2], $testData['c'] );
        $this->assertEquals( $yRespond['paths'][0][3]['id'], $testData['y']['id'] );
        $this->assertTrue( $yRespond['paths'][0][4] >= 1 );


        // Long path check
        $xCatData = explode( ".", $testData['x']['category'] );
        $xPathsJSON = $this->bridge->query( $testData['a'], $xCatData[0], $xCatData[1], $xCatData[2] );
        $xRespond = json_decode( $xPathsJSON, true );
        $this->assertEquals( $xRespond['paths'][0][0], $testData['a'] );
        $this->assertTrue( ( $xRespond['paths'][0][1] == $testData['c'] ) || ( $xRespond['paths'][0][1] == $testData['d'] ) );
        $this->assertEquals( $xRespond['paths'][0][2], $testData['f'] );
        $this->assertEquals( $xRespond['paths'][0][3]['id'], $testData['x']['id'] );
        $this->assertTrue( $xRespond['paths'][0][4] >= 2 );

        // Test get item by id
        $getItemByIdJSON = $this->bridge->getItemById( $testData['x']['id'] );
        $getItemById = json_decode( $getItemByIdJSON, true );
        // Test owner id
        $this->assertEquals( $getItemById['itemData'][0][0], $testData['f'] );
        // Test item id
        $this->assertEquals( $getItemById['itemData'][0][1]['id'], $testData['x']['id'] );

        // Test query items by owner node id
        $queryItemsByOwnerIdJSON = $this->bridge->queryItems( $testData['f'], null, null, null );
        $queryItemsByOwnerId = json_decode( $queryItemsByOwnerIdJSON, true );
        // Test owner id
        $this->assertEquals( $queryItemsByOwnerId['itemsData'][0][0], $testData['f'] );
        // Test item id
        $this->assertEquals( $queryItemsByOwnerId['itemsData'][0][1]['id'], $testData['x']['id'] );


        // Test query items by category
        $categoryXData = explode( ".", $testData['x']['category'] );
        $queryItemsByCategoryJSON = $this->bridge->queryItems( null, $categoryXData[0], $categoryXData[1], $categoryXData[2] );
        $queryItemsByCategory = json_decode( $queryItemsByCategoryJSON, true );
        // Test category
        $this->assertEquals( $queryItemsByCategory['itemsData'][0][1]['category'], $testData['x']['category'] );

        file_put_contents( 'testCreateSelectPath.log', print_r( $testData, true )."\n".$xPathsJSON."\n".$yPathsJSON."\n".$getItemByIdJSON."\n".$queryItemsByOwnerIdJSON."\n".$queryItemsByCategoryJSON."\n", FILE_APPEND );
    }

    /**
     * Read 1000 data items
     * @return array
     */
    public function provider()
    {
        $testData = array();
        for( $i=1; $i<=100; $i++ ){
            $testData[] = array(
                array(
                    'a' => rand( 1000000, 10000000 ),
                    'b' => rand( 1000000, 10000000 ),
                    'c' => rand( 1000000, 10000000 ),
                    'd' => rand( 1000000, 10000000 ),
                    'e' => rand( 1000000, 10000000 ),
                    'f' => rand( 1000000, 10000000 ),
                    'x' => $this->_makeItemData(),
                    'y' => $this->_makeItemData(),
                    'z' => $this->_makeItemData(),
                )
            );

        }
        return $testData;
    }

    private function _makeItemData( ){
        $category = "{$this->categories[ rand(1,30) ]}.{$this->categories[ rand(1,30) ]}.{$this->categories[ rand(1,30) ]}";
        $itemData = array(
            'id' => rand( 1000000, 10000000 ),
            "link_id" => rand( 10, 100 ),
            "owner_comment" => uniqid( "Comment:"),
            "created_time" => rand( 0, 1354233600 ),
            "url" => uniqid( "Url:"),
            "email" => uniqid( "Email:"),
            "title" => uniqid( "Title:"),
            "category" => $category,
            "img" => uniqid( "img:"),
            "rate" => uniqid( "rate:"),
            "reference" => uniqid( "reference:"),
            "city" => uniqid( "city:")
        );
        return $itemData;
    }

    public function setUp( ){
        $this->bridge = new Neo4jBridge();
    }
}
 