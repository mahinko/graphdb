<?php
/**
 *  
 *
 * @version 1.0
 * Date: 12/26/13
 * Time: 10:54 AM
 */

require_once( '__init__.php' );

ini_set('memory_limit','2000M');

class UpdateFriendsNeo4jBridgeTest extends PHPUnit_Framework_TestCase {

    private $bridge;

    /**
     * @dataProvider provider
     */
    public function testUpdateFriends( $nodeId, $friends ){
        $json = $this->bridge->updateFriends( $nodeId, $friends );
        file_put_contents( 'testUpdateFriends.log', $json."\n", FILE_APPEND );
        $respond = json_decode( $json, true );

        if( $respond['result'] == 'failure' ){
            echo $respond['errorMessage'];
        }
        $this->assertEquals( $respond['result'], 'success' );

    }

    /**
     * 1000 nodes update data
     * @return array
     */
    public function provider()
    {
        $data = array();
        for( $i=1; $i<=100; $i++ ){
            $friends = array();
            for( $j=1; $j<=250; $j++ ){
                $friends[] = rand( 1, 100000 );
            }
            $data[] = array(
              rand( 1, 100000 ),
              $friends
            );
        }
        return $data;
    }

    public function setUp( ){
        $this->bridge = new Neo4jBridge();
    }
}
 