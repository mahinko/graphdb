# wget install
yum install -y wget

# JDK install
wget -O jdk-7u45-linux-x64.rpm --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2Ftechnetwork%2Fjava%2Fjavase%2Fdownloads%2Fjdk7-downloads-1880260.html" "http://download.oracle.com/otn-pub/java/jdk/7u45-b18/jdk-7u45-linux-x64.rpm"
rpm -Uvh jdk-7u45-linux-x64.rpm

# Neo4j install
wget http://dist.neo4j.org/neo4j-enterprise-1.9.5-unix.tar.gz
mkdir /root/.neo4j
mkdir /root/.neo4j/last
tar -xvf neo4j-enterprise-1.9.5-unix.tar.gz -C /root/.neo4j/last/

echo "* hard nofile 40000">>/etc/security/limits.conf
echo "* soft nofile 40000">>/etc/security/limits.conf

yum install -y lsof

yum install -y php

# Open port
iptables -I INPUT -p tcp -m state --state NEW -m tcp --dport 7474 -j ACCEPT
service iptables save
service iptables restart

# Tune Neo4j settings
sed -i.bak "s/#org.neo4j.server.webserver.address=0.0.0.0/org.neo4j.server.webserver.address=0.0.0.0/g" /root/.neo4j/last/neo4j-enterprise-1.9.5/conf/neo4j-server.properties
cp Neo4jConf/neo4j.properties /root/.neo4j/last/neo4j-enterprise-1.9.5/conf/neo4j.properties
cp Neo4jConf/neo4j-wrapper.conf /root/.neo4j/last/neo4j-enterprise-1.9.5/conf/neo4j-wrapper.conf

echo "Need to reboot"
sleep 5
reboot