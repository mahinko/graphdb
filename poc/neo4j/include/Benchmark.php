<?php
/**
 *  
 *
 * @version 1.0
 * Date: 12/12/13
 * Time: 10:02 PM
 */


require_once('../lib/neo4jphp.phar');

use Everyman\Neo4j\Client,
    Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Batch,
    Everyman\Neo4j\Relationship,
    Everyman\Neo4j\Node,
    Everyman\Neo4j\Traversal,
    Everyman\Neo4j\Cypher;

date_default_timezone_set('UTC');

ini_set('memory_limit','8000M');

class Benchmark {

    private $neo4jHost = null;

    private $categories = array(
        1 => 'xayaza',
        2 => 'xbybzb',
        3 => 'xcyczc',
        4 => 'xdydzd',
        5 => 'xeyeze',
        6 => 'xfyfzf',
        7 => 'xgygzg',
        8 => 'xhyhzh',
        9 => 'xiyizi',
        10 => 'xjyjzj1',
        11 => 'xjyjzj2',
        12 => 'xjyjzj3',
        13 => 'xjyjzj4',
        14 => 'xjyjzj5',
        15 => 'xjyjzj6',
        16 => 'xjyjzj7',
        17 => 'xjyjzj8',
        18 => 'xjyjzj9',
        19 => 'xjyjzj10',
        20 => 'xjyjzj11',
        21 => 'xjyjzj12',
        22 => 'xjyjzj13',
        23 => 'xjyjzj14',
        24 => 'xjyjzj15',
        25 => 'xjyjzj16',
        26 => 'xjyjzj17',
        27 => 'xjyjzj18',
        28 => 'xjyjzj19',
        29 => 'xjyjzj20',
        30 => 'xjyjzj21',
    );

    private $neo4jConn;

    public function nestQuerySpawner( $nestNumber, $limit ){
        if( !empty( $nestNumber ) ){
            for( $i=1; $i<=$nestNumber; $i++ ){
                system( "nohup php runQuerySpawner.php $limit >> ../logs/NestSpawn.log 2>&1 &" );
                sleep( 1 );
            }
        }
    }

    public function makeQuerySpawner( $limit ){
        if( !empty( $limit ) ){
            for( $i=1; $i<=$limit; $i++ ){
                system( "nohup php makeAQuery.php >> ../logs/BenchmarkSpawn.log 2>&1 &" );
                sleep(1);
            }
        }
    }

    public function nestNodeASpawner( $nestNumber, $limit ){
        if( !empty( $nestNumber ) ){
            for( $i=1; $i<=$nestNumber; $i++ ){
                system( "nohup php runNodeASpawner.php $limit >> ../logs/NestSpawnNodeA.log 2>&1 &" );
                sleep(1);
            }
        }
    }

    public function makeNodeASpawner( $limit ){
        if( !empty( $limit ) ){
            for( $i=1; $i<=$limit; $i++ ){
                system( "nohup php makeNodeA.php >> ../logs/BenchmarkSpawnNodeA.log 2>&1 &" );
                sleep(1);
            }
        }
    }

    public function nestAddUpdateDeleteItemSpawner( $nestNumber, $limit ){
        if( !empty( $nestNumber ) ){
            for( $i=1; $i<=$nestNumber; $i++ ){
                system( "nohup php runDataItemAddUpdateDeleteSpawner.php $limit >> ../logs/NestDataItemAddUpdateDeleteSpawner.log 2>&1 &" );
                sleep(1);
            }
        }
    }

    public function makeAddUpdateDeleteItemSpawner( $limit ){
        if( !empty( $limit ) ){
            for( $i=1; $i<=$limit; $i++ ){
                system( "nohup php makeAddUpdateDeleteItem.php >> ../logs/BenchmarkSpawnAddUpdateDeleteItem.log 2>&1 &" );
                sleep(1);
            }
        }
    }

    public function makeQuerySerie( $count ){
        if( !empty( $count ) ){
            for( $i=1; $i<=$count; $i++ ){
                system( "nohup php makeAQuery.php >> ../logs/Benchmark_$count.log 2>&1 &" );
            }
        }
    }

    public function makeQuery( ){
        $idNodeA = rand( 1, 199000 );
        $category = 'cat1:'.$this->categories[ rand(1,30) ].'_cat2:'.$this->categories[ rand(1,30) ].'_cat3:'.$this->categories[ rand(1,30) ];

        $path = '';

        try {
            $queryString = "START nodeA=node:Nodes('id:$idNodeA'), item=node:Categories('category:\"".$category."\"') " .
                "MATCH path=nodeA-[*1..3]->item " .
                "RETURN DISTINCT path";

            $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString, array( 'category' => $category ) );
            $results = $query->getResultSet();

            if( !empty( $results ) ) {
                $path = "Path. Node A id : $idNodeA; Category: $category; Path Number:". count($results) ;
            }
        } catch ( Exception $e ) {
            echo $e->getMessage();
        }

        return "$path\n";
    }

    public function makeQuery2Index( $limit ){
        $idNodeA = rand( 1, $limit );
        $category = 'cat1:'.$this->categories[ rand(1,30) ].'_cat2:'.$this->categories[ rand(1,30) ].'_cat3:'.$this->categories[ rand(1,30) ];

        $path = '';

        try {
            $queryString = "START nodeA=node:Nodes('id:$idNodeA'), nodeAx=node:NodeCategoriesFull('category:\"".$category."\"'), item=node:Categories('category:\"".$category."\"') " .
                "MATCH path=nodeA-[:FRIEND]-node-[:FRIEND]-nodeAx-->item " .
                "RETURN DISTINCT path";

            $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString, array( 'category' => $category ) );
            $results = $query->getResultSet();

            if( !empty( $results ) ) {
                $path = "\nLong Path. Node A id : $idNodeA; Category: $category; Path Number:". count($results) ;
            }
        } catch ( Exception $e ) {
            echo $e->getMessage();
        }

        try {
            $queryString = "START nodeA=node:Nodes('id:$idNodeA'), nodeAx=node:NodeCategoriesFull('category:\"".$category."\"'), item=node:Categories('category:\"".$category."\"') " .
                "MATCH path=nodeA-[:FRIEND]-nodeAx-->item " .
                "RETURN DISTINCT path";

            $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString, array( 'category' => $category ) );
            $results = $query->getResultSet();

            if( !empty( $results ) ) {
                $path .= "\nShort Path. Node A id : $idNodeA; Category: $category; Path Number:". count($results) ;
            }
        } catch ( Exception $e ) {
            echo $e->getMessage();
        }

        return "$path\n";
    }

    public function makeNodeA(){
        $idNodeAIn = rand( 1, 20000000 );

        $nodeIndex = new Everyman\Neo4j\Index\NodeIndex($this->neo4jConn, 'Nodes');
        $match = $nodeIndex->findOne('id', $idNodeAIn);

        if( !empty( $match ) ){
            $nodeA = $match;
            $idNodeA = $nodeA->getId();
        }
        else{
            $nodeA = $this->neo4jConn->makeNode()->save();
            $nodeIndex->add( $nodeA, 'id', $idNodeAIn );
            $idNodeA = $nodeA->getId();
        }

        $this->neo4jConn->startBatch();
        for( $i=1; $i<=250; $i++ ){
            $idNodeX = rand( 1, 20000000 );

            $nodeX = $nodeIndex->findOne('id', $idNodeX );
            if( empty( $nodeX ) ){
                $nodeX = $this->neo4jConn->makeNode()->save();
                $nodeIndex->add( $nodeX, 'id', $idNodeX );

                $nodeA->relateTo( $nodeX, 'FRIEND' )->save();
            }
            else{
                $greyIdNodex = $nodeX->getId();

                $queryString = "START nodeA=node($idNodeA), nodeX=node($greyIdNodex) " .
                               "MATCH nodeA-[r:FRIEND]->nodeX ".
                               "RETURN r";

                $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
                $res = $query->getResultSet();

                if( empty( $res ) ){
                    $nodeA->relateTo( $nodeX, 'FRIEND' )->save();
                }
            }


        }
        $this->neo4jConn->commitBatch();


        return "Id node A: $idNodeA; Id Node A IN:$idNodeAIn";
    }

    public function makeAddUpdateDelete(){
        $randOperation = rand( 1, 3 );
        switch( $randOperation ){
            case 1: return $this->addDataItem();
            case 2: return $this->updateDataItem();
            case 3: return $this->deleteDataItem();
        }
        return "error";
    }

    public function updateDataItem( ){
        $idNodeA = rand( 1, 2000000 );

        $queryString = "START nodeA=node:Nodes('id:$idNodeA') " .
            "MATCH nodeA-[:DATA]->item " .
            "WHERE HAS(item.id) AND item.id > 0 " .
            "WITH item LIMIT 1 ".
            "SET item.title = \"".uniqid("Title:updated").'" ';

        try {
            $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
            $query->getResultSet();
        } catch ( Exception $e ) {
            return "Error item updating:\n $queryString";
        }
        return "Data item on node $idNodeA is UPDATED";
    }

    public function addDataItem( ){
        $idNodeA = rand( 1, 10200000 );

        $category = 'cat1:'.$this->categories[ rand(1,30) ].'_cat2:'.$this->categories[ rand(1,30) ].'_cat3:'.$this->categories[ rand(1,30) ];


        $queryString = "START nodeA=node:Nodes('id:$idNodeA') " .
            "CREATE UNIQUE nodeA-[:DATA]->(item {
            id:".rand( 50000000, 60000000 ).",
            link_id:".rand( 10, 100 ).",
            owner_comment:'".uniqid( "Comment:")."',
            created_time:".rand( 0, 1354233600 ).",
            url:'".uniqid( "Url:")."',
            email:'".uniqid( "Email:")."',
            title:'".uniqid( "Title:added")."',
            category:'".'level1.level2.'.$category."',
            img:'".uniqid( "img:")."',
            rate:'".uniqid( "rate:")."',
            reference:'".uniqid( "reference:")."',
            city:'".uniqid( "city:")."'
            })
            RETURN item
            ";

        try {
            $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
            $result = $query->getResultSet();
            if( !empty( $result ) ){
                foreach( $result as $row ){
                    $dataItemNode = $row['item'];
                    $categoryIndex = new Everyman\Neo4j\Index\NodeIndex( $this->neo4jConn, 'Categories' );
                    $categoryIndex->add( $dataItemNode, 'category', $category );
                }
            }

        } catch ( Exception $e ) {
            return "Error nodeA adding:\n$queryString";
        }
        return "Data item on node $idNodeA is ADDED";
    }

    public function deleteDataItem( ){
        $idNodeA = rand( 1, 2000000 );

        $queryString = "START nodeA=node:Nodes('id:$idNodeA') " .
            "MATCH nodeA-[:DATA]->item " .
            "WHERE HAS(item.id) AND item.id > 0 " .
            "WITH item LIMIT 1 ".
            "MATCH item-[r?]-() ".
            "DELETE item,r";

        try {
            $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
            $query->getResultSet();
        } catch ( Exception $e ) {
            return "Error item deletion:\n$queryString";
        }

        return "Data item on node $idNodeA is DELETED";
    }


    private function _init(){
        $this->neo4jConn = new Client( $this->neo4jHost );
    }

    public function __construct( ){
        $this->_init();
    }

} 