<?php
/**
 *  
 *
 * @version 1.0
 * Date: 12/12/13
 * Time: 10:02 PM
 */

namespace BridgeLiveData;

require_once('../lib/neo4jphp.phar');
require_once('../../../include/Neo4jBridge.php');

use Everyman\Neo4j\Client,
    Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Batch,
    Everyman\Neo4j\Relationship,
    Everyman\Neo4j\Node,
    Everyman\Neo4j\Traversal,
    Everyman\Neo4j\Cypher;

date_default_timezone_set('UTC');

ini_set('memory_limit','8000M');

class Benchmark {

    private $neo4jBridge;

    private $categories = array(
        1 => 'xayaza',
        2 => 'xbybzb',
        3 => 'xcyczc',
        4 => 'xdydzd',
        5 => 'xeyeze',
        6 => 'xfyfzf',
        7 => 'xgygzg',
        8 => 'xhyhzh',
        9 => 'xiyizi',
        10 => 'xjyjzj1',
        11 => 'xjyjzj2',
        12 => 'xjyjzj3',
        13 => 'xjyjzj4',
        14 => 'xjyjzj5',
        15 => 'xjyjzj6',
        16 => 'xjyjzj7',
        17 => 'xjyjzj8',
        18 => 'xjyjzj9',
        19 => 'xjyjzj10',
        20 => 'xjyjzj11',
        21 => 'xjyjzj12',
        22 => 'xjyjzj13',
        23 => 'xjyjzj14',
        24 => 'xjyjzj15',
        25 => 'xjyjzj16',
        26 => 'xjyjzj17',
        27 => 'xjyjzj18',
        28 => 'xjyjzj19',
        29 => 'xjyjzj20',
        30 => 'xjyjzj21',
    );

    private $neo4jConn;

    public function nestQuerySpawner( $nestNumber, $limit ){
        if( !empty( $nestNumber ) ){
            for( $i=1; $i<=$nestNumber; $i++ ){
                system( "nohup php runQuerySpawnerBridgeLiveData.php $limit >> ../logs/NestSpawnBridgeLiveData.log 2>&1 &" );
                sleep( 1 );
            }
        }
    }

    public function makeQuerySpawner( $limit ){
        if( !empty( $limit ) ){
            for( $i=1; $i<=$limit; $i++ ){
                system( "nohup php makeAQueryBridgeLiveData.php >> ../logs/BenchmarkSpawnBridgeLiveData.log 2>&1 &" );
                sleep(1);
            }
        }
    }

    public function nestNodeASpawner( $nestNumber, $limit ){
        if( !empty( $nestNumber ) ){
            for( $i=1; $i<=$nestNumber; $i++ ){
                system( "nohup php runNodeASpawnerBridgeLiveData.php $limit >> ../logs/NestSpawnNodeABridgeLiveData.log 2>&1 &" );
                sleep(1);
            }
        }
    }

    public function makeNodeASpawner( $limit ){
        if( !empty( $limit ) ){
            for( $i=1; $i<=$limit; $i++ ){
                system( "nohup php makeNodeABridgeLiveData.php >> ../logs/BenchmarkSpawnNodeABridgeLiveData.log 2>&1 &" );
                sleep(1);
            }
        }
    }

    public function nestAddUpdateDeleteItemSpawner( $nestNumber, $limit ){
        if( !empty( $nestNumber ) ){
            for( $i=1; $i<=$nestNumber; $i++ ){
                system( "nohup php runDataItemAddUpdateDeleteSpawnerBridgeLiveData.php $limit >> ../logs/NestDataItemAddUpdateDeleteSpawnerBridgeLiveData.log 2>&1 &" );
                sleep(1);
            }
        }
    }

    public function makeAddUpdateDeleteItemSpawner( $limit ){
        if( !empty( $limit ) ){
            for( $i=1; $i<=$limit; $i++ ){
                system( "nohup php makeAddUpdateDeleteItemBridgeLiveData.php >> ../logs/BenchmarkSpawnAddUpdateDeleteItemBridgeLiveData.log 2>&1 &" );
                sleep(1);
            }
        }
    }

    public function makeQuery( ){
        $randData = trim( static::randLine( '../assets/queryProviderLiveData.csv' ) );
        $randData = explode( ";", $randData );
        $idNodeA = $randData[0];
        $catData = explode( ".", $randData[1] );
        if( rand( 0, 100 ) < 33 ){
            $cat1 = $catData[0];
            $cat2 = $catData[1];
            $cat3 = $catData[2];
        }
        else{
            if( rand( 0, 100 ) < 50 ){
                $cat1 = $catData[0];
                $cat2 = $catData[1];
                $cat3 = '';
            }
            else{
                $cat1 = $catData[0];
                $cat2 = '';
                $cat3 = '';
            }
        }
        $paths = $this->neo4jBridge->query( $idNodeA, $cat1, $cat2, $cat3 );
        return "query($idNodeA, $cat1, $cat2, $cat3)\n$paths\n";
    }

    private static function randLine($fileName, $maxLineLength = 100000) {
        $handle = @fopen($fileName, "r");
        if ($handle) {
            $random_line = null;
            $line = null;
            $count = 0;
            while (($line = fgets($handle, $maxLineLength)) !== false) {
                $count++;
                if(rand() % $count == 0) {
                    $random_line = $line;
                }
            }
            if (!feof($handle)) {
                echo "Error: unexpected fgets() fail\n";
                fclose($handle);
                return null;
            } else {
                fclose($handle);
            }
            return $random_line;
        }
    }

    public function makeNodeA(){
        $nodeId = rand( 1, 20000000 );
        $friends = array();
        for( $j=1; $j<=250; $j++ ){
            $friends[] = rand( 1, 10000000 );
        }
        $json = $this->neo4jBridge->updateFriends( $nodeId, $friends );
        return "$json";
    }

    public function makeAddUpdateDelete(){
        $randOperation = rand( 1, 3 );
        switch( $randOperation ){
            case 1: return $this->addDataItem();
            case 2: return $this->updateDataItem();
            case 3: return $this->deleteDataItem();
        }
        return "error";
    }

    public function updateDataItem( ){
        $idsData = explode( ";", trim( static::randLine( '../assets/itemIdsLiveData.csv' ) ));
        $this->neo4jBridge->deleteItemById( $idsData[1] );
        $itemData = $this->_generateItem();
        $respond = $this->neo4jBridge->makeItem( $idsData[0], $itemData );
        return $respond;
    }

    public function addDataItem( ){
        $idsData = explode( ";", trim( static::randLine( '../assets/itemIdsLiveData.csv' ) ));
        $itemData = $this->_generateItem();
        $respond = $this->neo4jBridge->makeItem( $idsData[0], $itemData );
        return $respond;
    }

    private function _generateItem(){
        $category = "{$this->categories[ rand(1,30) ]}.{$this->categories[ rand(1,30) ]}.{$this->categories[ rand(1,30) ]}";
        $item = array(
            "owner_comment" => uniqid( "Comment:"),
            "created_time" => rand( 0, 1354233600 ),
            "url" => uniqid( "Url:"),
            "email" => uniqid( "Email:"),
            "title" => uniqid( "Title:"),
            "category" => $category,
            "img" => uniqid( "img:"),
            "rate" => uniqid( "rate:"),
            "city" => uniqid( "city:")
        );
        return $item;
    }

    public function deleteDataItem( ){
        $idsData = explode( ";", trim( static::randLine( '../assets/itemIdsLiveData.csv' ) ));
        $respond = $this->neo4jBridge->deleteItemById( $idsData[1] );
        return $respond;
    }

    public function updateDataItemProvider( $limit ){
        $ids = array();
        $queryString = "START item=node:Items('id:*') " .
            "MATCH c-[:DATA]->item ".
            "RETURN c.id, ID(item) LIMIT $limit";
        try {
            $query = new \Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
            $results = $query->getResultSet();
            if( !empty( $results ) ){
                foreach( $results as $row ){
                    $ids[] = $row['c.id'].";".$row['ID(item)'];
                }
            }
        } catch ( \Exception $e ) {
            echo "Error item ids reading:\n";
        }
        file_put_contents( '../assets/itemIdsLiveData.csv', implode( "\n", $ids ) );
    }

    public function updateQueryProvider( $limit ){
        $data = array();
        while( count( $data ) < $limit ){
            $rSkip = rand( 0, 500000 );
            $queryString = "START item=node:Items('id:*') " .
                "MATCH friend1-[:FRIEND]-friend2-[:DATA]->item ".
                "WHERE ID( friend2 ) > $rSkip ".
                "RETURN friend1.id, item.category ".
                "LIMIT 10";
            try {
                $query = new \Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
                $results = $query->getResultSet();
                if( !empty( $results ) ){
                    foreach( $results as $row ){
                        $data[] = $row['ID(friend1)'].";".$row['item.category'];
                    }
                }
            } catch ( \Exception $e ) {
                echo $e->getMessage();
            }
        }
        file_put_contents( '../assets/queryProviderLiveData.csv', implode( "\n", $data ) );
    }

    private function _init(){
        $this->neo4jBridge = new \Neo4jBridge();
        $this->neo4jConn = $this->neo4jBridge->neo4jConn;
    }

    public function __construct( ){
        $this->_init();
    }

}