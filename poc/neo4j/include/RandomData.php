<?php
/**
 *  
 *
 * @version 1.0
 * Date: 12/11/13
 * Time: 12:07 PM
 */

require_once('../lib/neo4jphp.phar');
require_once('../../../include/Neo4jBridge.php');

use Everyman\Neo4j\Client,
    Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Batch,
    Everyman\Neo4j\Relationship,
    Everyman\Neo4j\Node,
    Everyman\Neo4j\Cypher;

date_default_timezone_set('UTC');

ini_set('memory_limit','16000M');

class RandomData {
    private $neo4jConn;
    private $itemId = 1;

    private $categories = array(
            1 => 'xayaza',
            2 => 'xbybzb',
            3 => 'xcyczc',
            4 => 'xdydzd',
            5 => 'xeyeze',
            6 => 'xfyfzf',
            7 => 'xgygzg',
            8 => 'xhyhzh',
            9 => 'xiyizi',
            10 => 'xjyjzj1',
            11 => 'xjyjzj2',
            12 => 'xjyjzj3',
            13 => 'xjyjzj4',
            14 => 'xjyjzj5',
            15 => 'xjyjzj6',
            16 => 'xjyjzj7',
            17 => 'xjyjzj8',
            18 => 'xjyjzj9',
            19 => 'xjyjzj10',
            20 => 'xjyjzj11',
            21 => 'xjyjzj12',
            22 => 'xjyjzj13',
            23 => 'xjyjzj14',
            24 => 'xjyjzj15',
            25 => 'xjyjzj16',
            26 => 'xjyjzj17',
            27 => 'xjyjzj18',
            28 => 'xjyjzj19',
            29 => 'xjyjzj20',
            30 => 'xjyjzj21',
    );

    /**
     * Makes all friends nodes and data intems on basis of Neo4jBridge method
     */
    public function fillFullDB( ){
        //$this->_addFriends();
        $this->_addDataItems();
    }

    /**
     * Add data item nodes on basis Neo4jBridge makeItem method
     */
    private function _addDataItems( ){
        $bridge = new Neo4jBridge();
        $counter = 0;
        for( $i=1; $i<=200000; $i++ ){

            $itemsCount = 3 + rand( 0, 1 ) - rand( 0, 1 );
            for( $j=1; $j <= $itemsCount; $j++ ){
                $itemData = $this->_generateItem();
                $respond = $bridge->makeItem( $i, $itemData );
                $respond = json_decode( $respond, true );
                if( !empty( $respond['errorMessage'] ) ){
                    echo $respond['errorMessage'];
                }
            }

            if( $counter > 500 ){
                $progress = round( ($i/200000)*100, 2 );
                echo "$i - $progress%\n";
                $counter = 0;
                unset( $bridge );
                $bridge = new Neo4jBridge();
            }

            $counter++;
        }
    }

    private function _generateItem(){
        $category = "{$this->categories[ rand(1,30) ]}.{$this->categories[ rand(1,30) ]}.{$this->categories[ rand(1,30) ]}";
        $item = array(
            "owner_comment" => uniqid( "Comment:"),
            "created_time" => rand( 0, 1354233600 ),
            "url" => uniqid( "Url:"),
            "email" => uniqid( "Email:"),
            "title" => uniqid( "Title:"),
            "category" => $category,
            "img" => uniqid( "img:"),
            "rate" => uniqid( "rate:"),
            "city" => uniqid( "city:")
        );
        return $item;
    }

    /**
     * Add friends nodes on basis Neo4jBridge updateFriend method
     */
    private function _addFriends( ){
        $bridge = new Neo4jBridge();
        $counter = 0;
        for( $i=1; $i<=200000; $i++ ){
            $friends = array();
            for( $j=1; $j<=250; $j++ ){
                $friends[] = rand( 1, 10000000 );
            }
            $respond = $bridge->updateFriends( $i, $friends );
            $respond = json_decode( $respond, true );
            if( !empty( $respond['errorMessage'] ) ){
                echo $respond['errorMessage'];
            }
            if( $counter > 500 ){
                $progress = round( ($counter/$i)*100, 2 );
                echo "$i - $progress%\n";
                $counter = 0;
                unset( $bridge );
                $bridge = new Neo4jBridge();
            }
        }
    }

    public function addNodesB( $count ){

        echo "Add $count nodes\n";
        $batchCounter = 0;

        $this->neo4jConn->startBatch();
        for( $i=1; $i < $count; $i++ ){
            $this->_addNode( $i, $i );

            if( $batchCounter >= 10000 ){
                $this->neo4jConn->commitBatch();

                unset( $nodesIndex );
                unset( $this->neo4jConn );
                $this->_init();

                $this->neo4jConn->startBatch();
                $batchCounter = 0;
                $completePercent = round( ($i/$count)*100, 2);
                echo "$completePercent%\n";
            }
            $batchCounter++;
        }
        $this->neo4jConn->commitBatch();
    }

    private function _addNode( $id, $timestamp ){
        $node = $this->neo4jConn->makeNode();
        $node->setProperty( 'id', $id )
            ->setProperty( 'timeOfLastRelationshipsUpdate', $timestamp )
            ->save();
    }

    public function addAllNodesToIndex( $count ){

        echo "Add $count nodes to Nodes index\n";

        $nodesIndex = new Everyman\Neo4j\Index\NodeIndex( $this->neo4jConn, 'Nodes');
        $nodesIndex->save();
        $batchCounter = 0;

        $this->neo4jConn->startBatch();
        for( $i=1; $i < $count; $i++ ){

            $node = $this->neo4jConn->getNode( $i );
            if( $node ){
                $nodesIndex->add( $node, 'id', $i );
            }

            if( $batchCounter >= 10000 ){
                $this->neo4jConn->commitBatch();
                unset( $nodesIndex );

                unset( $this->neo4jConn );
                $this->_init();

                $nodesIndex = new Everyman\Neo4j\Index\NodeIndex( $this->neo4jConn, 'Nodes');
                $this->neo4jConn->startBatch();
                $batchCounter = 0;
                $completePercent = round( ($i/$count)*100, 2);
                echo "$completePercent%\n";
            }
            $batchCounter++;
        }
        $this->neo4jConn->commitBatch();
    }

    /**
     * Add 1 relationships from every node to nodes of Type A. For simplicity nodes type A has ID in range 0..200 000
     * @param $count
     */
    public function addSmallRelationships( $count ){
        echo "Add one relationships from each node B to nodes A\n";
        $batchCounter = 0;
        $this->neo4jConn->startBatch();
        for( $i=1; $i < $count; $i++ ){

            $nodeB = $this->neo4jConn->getNode( $i );
            if( $nodeB ){
                $nodeA = $this->neo4jConn->getNode( rand( 1, 200000 ) );
                if( $nodeA ){
                    $nodeB->relateTo( $nodeA, 'FRIEND' )->save();
                }
            }

            if( $batchCounter >= 10000 ){
                try {
                    $this->neo4jConn->commitBatch();
                } catch ( Exception $e ) {
                    echo $e->getMessage();
                }

                unset( $this->neo4jConn );
                $this->_init();

                try {
                    $this->neo4jConn->startBatch();
                } catch ( Exception $e ) {
                    echo $e->getMessage();
                }
                $batchCounter = 0;
                $completePercent = round( ($i/$count)*100, 2);
                echo "$completePercent%\n";
            }
            $batchCounter++;

        }
        try {
            $this->neo4jConn->commitBatch();
        } catch ( Exception $e ) {
            echo $e->getMessage();
        }
    }

    /**
     * Add data items for first $count nodes
     * @param $count
     */
    public function addDataItems( $count ){
        echo "Add data items\n";
        $batchCounter = 0;
        for( $i=1; $i < $count; $i++ ){
            try {
                $this->neo4jConn->startBatch();
            } catch ( Exception $e ) {
                echo $e->getMessage();
            }
            $nodeA = $this->neo4jConn->getNode( $i );
            if( $nodeA ){
                $itemsCount = 3 + rand( 0, 1 ) - rand( 0, 1 );
                for( $j=1; $j <= $itemsCount; $j++ ){
                    $this->addItem( $nodeA );
                }
            }
            try {
                $this->neo4jConn->commitBatch();
            } catch ( Exception $e ) {
                echo $e->getMessage();
            }

            $batchCounter++;
            if( $batchCounter > 4000 ){
                $completePercent = round( ($i/$count)*100, 2);
                echo "$completePercent%\n";
                $batchCounter = 0;
                unset( $this->neo4jConn );
                $this->_init();
            }
        }

    }

    private function addItem( $nodeA ){

        $category = 'cat1:'.$this->categories[ rand(1,30) ].'_cat2:'.$this->categories[ rand(1,30) ].'_cat3:'.$this->categories[ rand(1,30) ];

        try {
            $nodeItem = $this->neo4jConn->makeNode();
            $nodeItem->setProperty( "id", $this->itemId )
                ->setProperty( "link_id", rand( 10, 100 ) )
                ->setProperty( "owner_comment", uniqid( "Comment:") )
                ->setProperty( "created_time", rand( 0, 1354233600 ) )
                ->setProperty( "url", uniqid( "Url:") )
                ->setProperty( "email", uniqid( "Email:") )
                ->setProperty( "title", uniqid( "Title:") )
                ->setProperty( "category", $category )
                ->setProperty( "img", uniqid( "img:") )
                ->setProperty( "rate", uniqid( "rate:") )
                ->setProperty( "reference", uniqid( "reference:") )
                ->setProperty( "city", uniqid( "city:") )
                ->save();
            $nodeA->relateTo( $nodeItem, 'DATA' )->save();
            $this->itemId++;
        } catch ( Exception $e ) {
            echo $e->getMessage();
        }
    }

    /**
     * $count - it is high limit of nodes Id which are processed. By default it is first 200K nodes
     * @param $count
     * @param $desiredAverageRelationsCount
     */
    public function addMoreRelationsToNodeA( $count, $desiredAverageRelationsCount ){

        echo "Add relationships for nodes A up to level $desiredAverageRelationsCount\m";
        $batchCounter = 0;
        $start = 1;

        for( $i=$start; $i < $count; $i++ ){
            $factRelCount = $this->_getFactRelCount( $i );
            $toSendCount = $desiredAverageRelationsCount - $factRelCount + rand( 0, 10 );
            if( $toSendCount < 0 ){
                $toSendCount = 0;
            }

            if( !empty( $toSendCount ) ){
                try{
                    $this->_makeRelationships( $i, $toSendCount );
                }
                catch( Exception $e ){
                    echo $e->getMessage();
                }
            }
            $batchCounter++;
            if( $batchCounter > 1000 ){
                $completePercent = round( ($i/$count)*100, 2);
                echo "$completePercent%\n";
                $batchCounter = 0;
                unset( $this->neo4jConn );
                $this->_init();
            }
        }
    }

    private function _makeRelationships( $id, $toSendCount ){
        $aNode = $this->neo4jConn->getNode( $id );
        $this->neo4jConn->startBatch();
        $randStack = array();
        if( $aNode ){
            for( $i=1; $i<=$toSendCount; $i++ ){
                $rnd = rand( $id + 1, 200000 );
                if( !isset( $randStack[ $rnd ] ) ){
                    $xNode = $this->neo4jConn->getNode( $rnd );
                    if( $xNode ){
                        $aNode->relateTo( $xNode, 'FRIEND' )->save();
                    }
                    $randStack[ $rnd ] = 1;
                }
            }
        }
        $this->neo4jConn->commitBatch();
    }

    private function _getFactRelCount( $id ){
        $queryString = "START a=node($id) ".
                       "MATCH a-[r]-() ".
                       "RETURN COUNT(r) as relCount";
        $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
        $results = $query->getResultSet();
        if( !empty( $results ) ){
            foreach( $results as $row ){
                return $row['relCount'];
            }
        }
        return 0;
    }

    public function indexCategories( ){
        echo "Add data items to Categories index\n";
        $categoryIndex = new Everyman\Neo4j\Index\NodeIndex( $this->neo4jConn, 'Categories' );
        $categoryIndex->save();
        $batchCounter = 0;

        $this->neo4jConn->startBatch();
        for( $i=1; $i < 200000; $i++ ){

            $queryString = "START a=node($i) ".
                "MATCH a-[:DATA]->data ".
                "RETURN data, data.category";
            $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
            $results = $query->getResultSet();
            if( !empty( $results ) ){
                foreach( $results as $row ){
                    $category = $row['data.category'];
                    $dataNode = $row['data'];
                    $categoryIndex->add( $dataNode, 'category', $category );
                }
            }

            if( $batchCounter >= 10000 ){
                try {
                    $this->neo4jConn->commitBatch();
                } catch ( Exception $e ) {
                    echo $e->getMessage();
                }
                unset( $categoryIndex );

                unset( $this->neo4jConn );
                $this->_init();

                $categoryIndex = new Everyman\Neo4j\Index\NodeIndex( $this->neo4jConn, 'Categories' );
                try {
                    $this->neo4jConn->startBatch();
                } catch ( Exception $e ) {
                    echo $e->getMessage();
                }
                $batchCounter = 0;
                $completePercent = round( ($i/200000)*100, 2);
                echo "$completePercent%\n";
            }
            $batchCounter++;
        }
        try {
            $this->neo4jConn->commitBatch();
        } catch ( Exception $e ) {
            echo $e->getMessage();
        }
    }

    public function indexNodeCategories( ){
        echo "Add nodes A to Categories index\n";
        $categoryIndex = new Everyman\Neo4j\Index\NodeIndex( $this->neo4jConn, 'NodeCategoriesFull' );
        $categoryIndex->save();
        $batchCounter = 0;

        $this->neo4jConn->startBatch();
        for( $i=1; $i < 200000; $i++ ){

            $queryString = "START a=node($i) ".
                "MATCH a-[:DATA]->data ".
                "RETURN a, data.category";
            $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
            $results = $query->getResultSet();
            if( !empty( $results ) ){
                foreach( $results as $row ){
                    $category = $row['data.category'];
                    $aNode = $row['data'];
                    $categoryIndex->add( $aNode, 'category', $category );
                }
            }

            if( $batchCounter >= 10000 ){
                try {
                    $this->neo4jConn->commitBatch();
                } catch ( Exception $e ) {
                    echo $e->getMessage();
                }
                unset( $categoryIndex );

                unset( $this->neo4jConn );
                $this->_init();

                $categoryIndex = new Everyman\Neo4j\Index\NodeIndex( $this->neo4jConn, 'NodeCategoriesFull' );
                try {
                    $this->neo4jConn->startBatch();
                } catch ( Exception $e ) {
                    echo $e->getMessage();
                }
                $batchCounter = 0;
                $completePercent = round( ($i/200000)*100, 2);
                echo "$completePercent%\n";
            }
            $batchCounter++;
        }
        try {
            $this->neo4jConn->commitBatch();
        } catch ( Exception $e ) {
            echo $e->getMessage();
        }
    }

    private function _init(){
        $this->neo4jConn = new Client();
    }

    public function __construct( ){
        $this->_init();
    }

} 