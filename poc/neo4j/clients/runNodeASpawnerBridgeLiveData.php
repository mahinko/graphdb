<?php
/**
 *  
 *
 * @version 1.0
 * Date: 12/13/13
 * Time: 12:20 PM
 */

require_once( '../include/BenchmarkBridgeLiveData.php' );

if( isset( $argv[1] ) ){
    $limit = $argv[1];
}
else{
    $limit = 1;
}

$runner = new BridgeLiveData\Benchmark();
$runner->makeNodeASpawner( $limit );