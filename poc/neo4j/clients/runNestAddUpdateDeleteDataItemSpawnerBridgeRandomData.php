<?php
/**
 *  
 *
 * @version 1.0
 * Date: 12/13/13
 * Time: 12:22 PM
 */

require_once( '../include/BenchmarkBridgeRandomData.php' );

if( isset( $argv[1] ) ){
    $nestNumber = $argv[1];
}
else{
    $nestNumber = 1;
}

if( isset( $argv[2] ) ){
    $limit = $argv[2];
}
else{
    $limit = 1;
}

$runner = new BridgeRandomData\Benchmark();
$runner->nestAddUpdateDeleteItemSpawner( $nestNumber, $limit );