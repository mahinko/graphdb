<?php
/**
 *  Launch n threads. Each thread sends m requests with 1 second delay. When all requests are sent the thread is shut down.
 *
 *  Usage: runNestNodeASpawner.php n m
 *
 * @version 1.0
 * Date: 12/13/13
 * Time: 12:22 PM
 */

require_once( '../include/Benchmark.php' );

if( isset( $argv[1] ) ){
    $nestNumber = $argv[1];
}
else{
    $nestNumber = 1;
}

if( isset( $argv[2] ) ){
    $limit = $argv[2];
}
else{
    $limit = 1;
}

$runner = new Benchmark();
$runner->nestQuerySpawner( $nestNumber, $limit );