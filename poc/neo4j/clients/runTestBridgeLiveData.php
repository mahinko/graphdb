<?php
/**
 *  
 *
 * @version 1.0
 * Date: 12/15/13
 * Time: 10:50 PM
 */


$querySpots = 1;
$limit = 100;

require_once( "../include/BenchmarkBridgeLiveData.php" );

echo "Preparing data for testing...";
$bench = new BridgeLiveData\Benchmark();
$bench->updateQueryProvider( $limit*$querySpots*2 );
$bench->updateDataItemProvider( $limit*2 );
echo "Done.\nSend jobs\n";

system( "nohup php runNestQuerySpawnerBridgeLiveData.php $querySpots $limit &" );
system( "nohup php runNestNodeASpawnerBridgeLiveData.php 2 $limit &" );
system( "nohup php runNestAddUpdateDeleteDataItemSpawnerBridgeLiveData.php 2 $limit &" );

echo "$querySpots query spots send; 2 node A spots added; 2 data item updater. Jobs limit $limit\n";