<?php
/**
 *  
 *
 * @version 1.0
 * Date: 12/14/13
 * Time: 3:11 PM
 */

require_once( '../include/Benchmark.php' );

$time_start = microtime(true);
$runner = new Benchmark();
$res = $runner->makeAddUpdateDelete();
$time_end = microtime(true);
$time = $time_end - $time_start;
$date = date('Y-m-d H:i:s');
echo "$date; $res, Time: $time seconds\n";