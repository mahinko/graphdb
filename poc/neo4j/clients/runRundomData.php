<?php
/**
 *  
 *
 * @version 1.0
 * Date: 12/11/13
 * Time: 12:54 PM
 */

require_once('../include/RandomData.php');

$runner = new RandomData();
$runner->addNodesB( 10200000 );
$runner->addAllNodesToIndex( 10200000 );
$runner->addSmallRelationships( 10200000 );
$runner->addMoreRelationsToNodeA( 200000, 250 );
$runner->addDataItems( 200000 );
$runner->indexCategories( 200000 );
$runner->indexNodeCategories( 200000 );

echo "Done";