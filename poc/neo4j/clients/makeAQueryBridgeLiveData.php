<?php
/**
 *  
 *
 * @version 1.0
 * Date: 12/12/13
 * Time: 11:35 PM
 */

require_once( '../include/BenchmarkBridgeLiveData.php' );

$time_start = microtime(true);
$runner = new BridgeLiveData\Benchmark();

$res = $runner->makeQuery();

$time_end = microtime(true);
$time = $time_end - $time_start;
$date = date('Y-m-d H:i:s');
echo "$date; $res\nTime: $time seconds\n\n";
