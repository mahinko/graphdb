<?php
/**
 *  
 *
 * @author Mike
 * @version 1.0
 * Date: 12/8/13
 * Time: 1:00 PM
 */

require_once('../includes/ImportMySQL.php');
ini_set('memory_limit','8000M');

$runner = new ImportMySQL();
$runner->addDataItems();
$runner->makeFriendsCSV();