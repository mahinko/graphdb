DROP TABLE IF EXISTS `data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data` (
  `id` bigint(20) unsigned NOT NULL,
  `dataId` smallint(5) unsigned NOT NULL,
  `category` varchar(256) NOT NULL,
  `date` date NOT NULL,
  KEY `owner_cat_date` (`id`,`category`,`date`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

DROP TABLE IF EXISTS `friends`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `friends` (
  `uid1` bigint(20) unsigned NOT NULL,
  `uid2` bigint(20) unsigned NOT NULL,
  UNIQUE KEY `uid1_2` (`uid1`,`uid2`),
  KEY `uid1` (`uid1`),
  KEY `uid2` (`uid2`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
