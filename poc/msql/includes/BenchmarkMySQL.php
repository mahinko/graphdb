<?php
/**
 *  
 *
 * @version 1.0
 * Date: 12/12/13
 * Time: 10:02 PM
 */

require_once('MySQLCategories.php');

date_default_timezone_set('UTC');
ini_set('memory_limit','8000M');

class BenchmarkMySQL {

    static private $dbHost = 'localhost';
    private $mysqli;


    public function nestQuerySpawner( $nestNumber, $limit ){
        if( !empty( $nestNumber ) ){
            for( $i=1; $i<=$nestNumber; $i++ ){
                system( "nohup php runQuerySpawnerMySQL.php $limit >> ../logs/NestSpawnMySQL.log 2>&1 &" );
                sleep( 1 );
            }
        }
    }

    public function makeQuerySpawner( $limit ){
        if( !empty( $limit ) ){
            for( $i=1; $i<=$limit; $i++ ){
                system( "nohup php makeAQueryMySQL.php >> ../logs/BenchmarkSpawnMySQL.log 2>&1 &" );
                sleep(1);
            }
        }
    }

    public function nestNodeASpawner( $nestNumber, $limit ){
        if( !empty( $nestNumber ) ){
            for( $i=1; $i<=$nestNumber; $i++ ){
                system( "nohup php runNodeASpawnerMySQL.php $limit >> ../logs/NestSpawnNodeAMySQL.log 2>&1 &" );
                sleep(1);
            }
        }
    }

    public function makeNodeASpawner( $limit ){
        if( !empty( $limit ) ){
            for( $i=1; $i<=$limit; $i++ ){
                system( "nohup php makeNodeAMySQL.php >> ../logs/BenchmarkSpawnNodeAMySQL.log 2>&1 &" );
                sleep(1);
            }
        }
    }

    public function nestAddUpdateDeleteItemSpawner( $nestNumber, $limit ){
        if( !empty( $nestNumber ) ){
            for( $i=1; $i<=$nestNumber; $i++ ){
                system( "nohup php runDataItemAddUpdateDeleteSpawnerMySQL.php $limit >> ../logs/NestDataItemAddUpdateDeleteSpawnerMySQL.log 2>&1 &" );
                sleep(1);
            }
        }
    }

    public function makeAddUpdateDeleteItemSpawner( $limit ){
        if( !empty( $limit ) ){
            for( $i=1; $i<=$limit; $i++ ){
                system( "nohup php makeAddUpdateDeleteItemMySQL.php >> ../logs/BenchmarkSpawnAddUpdateDeleteItemMySQL.log 2>&1 &" );
                sleep(1);
            }
        }
    }

    public function makeQuery( ){
        $idNodeA = rand( 1, 199000 );
        $category = MySQLCategories::getRandCategory();

        $path = '';

        try {

            $SQL = "
                SELECT *
                FROM (
                    # 1 short straight
                    SELECT fA_1.uid1 as nodeA, d1_1.id as friend1Id, NULL as friend2Id, d1_1.dataId as dataId
                    FROM friends fA_1
                    LEFT JOIN data d1_1
                    ON d1_1.id = fA_1.uid2
                    WHERE fA_1.uid1 = $idNodeA
                    AND ( d1_1.category = '$category' )

                    UNION ALL

                    # 2 short reverse
                    SELECT fA_2.uid2 as nodeA, fA_2.uid1 as friend1Id, NULL as friend2Id, d1_2.dataId as dataId
                    FROM friends fA_2
                    LEFT JOIN data d1_2
                    ON d1_2.id = fA_2.uid1
                    WHERE fA_2.uid2 = $idNodeA
                    AND ( d1_2.category = '$category' )

                    UNION ALL

                    # 3 long straight
                    SELECT fA_3.uid1 as nodeA, f1_3.uid1 as friend1Id, d1_3.id as friend2Id, d1_3.dataId as dataId
                    FROM friends fA_3
                    LEFT JOIN friends f1_3
                    ON fA_3.uid2 = f1_3.uid1
                    LEFT JOIN data d1_3
                    ON d1_3.id = f1_3.uid2
                    WHERE fA_3.uid1 = $idNodeA
                    AND ( d1_3.category = '$category' )

                    UNION ALL

                    # 4 long reverse
                    SELECT fA_4.uid2 as nodeA, f1_4.uid1 as friend1Id, d1_4.id as friend2Id, d1_4.dataId as dataId
                    FROM friends fA_4
                    LEFT JOIN friends f1_4
                    ON fA_4.uid1 = f1_4.uid1
                    LEFT JOIN data d1_4
                    ON d1_4.id = f1_4.uid2
                    WHERE fA_4.uid2 = $idNodeA
                    AND ( d1_4.category = '$category' )

                    UNION ALL

                    # 5 long reverse 2
                    SELECT fA_5.uid1 as NodeA, f1_5.uid2 as friend1, d1_5.id as friend2Id, d1_5.dataId as dataId
                    FROM friends fA_5
                    LEFT JOIN friends f1_5
                    ON fA_5.uid2 = f1_5.uid2
                    LEFT JOIN data d1_5
                    ON d1_5.id = f1_5.uid1
                    WHERE fA_5.uid1 = $idNodeA
                    AND ( d1_5.category = '$category' )

                    UNION ALL

                    # 6 long reverse 3
                    SELECT fA_6.uid2 as NodeA, f1_6.uid2 as friend1, d1_6.id as friend2Id, d1_6.dataId as dataId
                    FROM friends fA_6
                    LEFT JOIN friends f1_6
                    ON fA_6.uid1 = f1_6.uid2
                    LEFT JOIN data d1_6
                    ON d1_6.id = f1_6.uid1
                    WHERE fA_6.uid2 = $idNodeA
                    AND ( d1_6.category = '$category' )

                ) paths
            ";

            ( $result = $this->mysqli->query( $SQL ) ) or die( mysqli_error( $this->mysqli ) );

            if( !empty( $result ) ) {
                $path = "Path. Node A id : $idNodeA; Category: $category; Path Number:". $result->num_rows;
                $result->close();
            }
        } catch ( Exception $e ) {
            echo $e->getMessage();
        }

        return "$path\n";
    }


    public function makeNodeA(){

        // Create data records
        $idNodeA = rand( 1, 20000000 );
        $cat1 = MySQLCategories::getRandCategory();
        $cat2 = MySQLCategories::getRandCategory();
        $cat3 = MySQLCategories::getRandCategory();
        $SQL = "
        INSERT IGNORE INTO data ( `id`, `dataId`, `category`, `date` )
        VALUES ( $idNodeA, 0, '$cat1', '2011-01-01' ),
               ( $idNodeA, 1, '$cat2', '2011-02-01' ),
               ( $idNodeA, 2, '$cat3', '2011-03-01' )
        ";
        ( mysqli_query( $this->mysqli, $SQL ) ) or die( mysqli_error( $this->mysqli ) );

        // Create friends records
        $values = '';
        for( $i=1; $i<=250; $i++ ){
            $idNodeX = rand( 1, 20000000 );
            if( $idNodeA < $idNodeX ){
                $values .= "( $idNodeA, $idNodeX ),";
            }
            else{
                $values .= "( $idNodeX, $idNodeA ),";
            }
        }
        $values = rtrim( $values, ',' );
        $SQL = "INSERT IGNORE INTO friends ( `uid1`, `uid2` ) VALUES $values";
        ( mysqli_query( $this->mysqli, $SQL ) ) or die( mysqli_error( $this->mysqli ) );

        return "Id node A: $idNodeA";
    }

    public function makeAddUpdateDelete(){
        $randOperation = rand( 1, 3 );
        switch( $randOperation ){
            case 1: return $this->addDataItem();
            case 2: return $this->updateDataItem();
            case 3: return $this->deleteDataItem();
        }
        return "error";
    }

    public function updateDataItem( ){
        $idNodeA = rand( 1, 200000 );
        $SQL = "
        UPDATE data
        SET date = '2011-03-03'
        WHERE id=$idNodeA AND dataID = 0
        LIMIT 1
        ";
        ( mysqli_query( $this->mysqli, $SQL ) ) or die( mysqli_error( $this->mysqli ) );
        return "Data item on node $idNodeA is UPDATED";
    }

    public function addDataItem( ){
        $idNodeA = rand( 1, 10200000 );
        $cat = MySQLCategories::getRandCategory();
        $SQL = "
        INSERT IGNORE INTO data ( `id`, `dataId`, `category`, `date` )
        VALUES ( $idNodeA, 0, '$cat', '2011-01-01' )
        ";
        ( mysqli_query( $this->mysqli, $SQL ) ) or die( mysqli_error( $this->mysqli ) );

        return "Data item on node $idNodeA is ADDED";
    }

    public function deleteDataItem( ){
        $idNodeA = rand( 1, 200000 );
        $SQL = "
        DELETE FROM data
        WHERE id = $idNodeA AND dataId = 0
        LIMIT 1
        ";
        ( mysqli_query( $this->mysqli, $SQL ) ) or die( mysqli_error( $this->mysqli ) );
        return "Data 0 item on node $idNodeA is DELETED";
    }


    private function _init(){
        $this->mysqli = new mysqli( self::$dbHost, 'root', null, 'initdb' );
    }

    public function __construct( ){
        $this->_init();
    }

} 