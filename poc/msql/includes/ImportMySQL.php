<?php
/**
 *  
 *
 * @version 1.0
 * Date: 12/19/13
 * Time: 2:46 PM
 */

class ImportMySQL {

    static private $dbHost = 'localhost';

    private $mysqli;

    public function addDataItems( ){
        echo "Filling data table\n";
        $rows = $this->_readData( '../initdb/db.json' );

        $counter = 0;
        $i = 0;
        if( !empty( $rows ) ){
            $countRow = count( $rows );
            file_put_contents( 'categories', '' );
            foreach( $rows as $row ){
                $this->_addBlinks( $row );

                $counter++;
                $i++;

                if( $i > 1000 ){
                    $progtess = round( ( ( $counter/$countRow )*100 ), 2 );
                    echo "$progtess%\n";
                    $i = 0;
                }
            }

            $categories = array_unique( explode( "\n", file_get_contents( 'categories' ) ) );
            file_put_contents( 'categories', implode( "\n", $categories ) );
        }
        echo "Done";

    }

    public function makeFriendsCSV( $path = null ){
        echo "Generation friends csv\n";

        if( empty( $path ) ){
            $path = '../initdb/db.json';
        }

        $rows = $this->_readData( $path );

        $counter = 0;
        $i = 0;

        file_put_contents( 'friends.csv', "uid1,uid2\n" );

        $strings = '';

        if( !empty( $rows ) ){
            $rowsCount = count( $rows );
            foreach( $rows as $row ){

                $strings .= $this->_friendsToCSV( $row );

                $counter++;
                $i++;

                if( $i > 1000 ){
                    $progress = round( ($counter/$rowsCount)*100, 2 );
                    echo "$progress%\n";
                    $i = 0;
                    file_put_contents( 'friends.csv', $strings, FILE_APPEND | LOCK_EX );
                    $strings = '';
                }

            }

            if( !empty( $strings ) ){
                file_put_contents( 'friends.csv', $strings, FILE_APPEND | LOCK_EX );
            }

        }
        echo "Done";
    }

    private function _addBlinks( $row ){
        $nodeData = json_decode( $row, true );
        $categories = array();
        if( is_array( $nodeData ) ){
            $id = $nodeData['id'];
            $data = $nodeData['data'];
            if( !empty( $data ) ){
                foreach( $data as $dataItem ){
                    $dataId = $dataItem['id'];
                    $dataCategory = $dataItem['category'];
                    $categories[$dataCategory] = 1;
                    $dateData = explode( '/', $dataItem['date'] );
                    $date = $dateData[2].'-'.$dateData[1].'-'.$dateData[0];

                    $values = '';
                    $values .= "( $id, $dataId,  '$dataCategory', '$date' ),";
                    $values = rtrim( $values, ',' );

                    if( !empty( $values ) ){
                        $SQL = "INSERT IGNORE INTO data ( `id`, `dataId`, `category`, `date` ) VALUES $values";

                        ( mysqli_query( $this->mysqli, $SQL ) ) or die( mysqli_error( $this->mysqli ) );
                    }
                }
            }
        }

        if( !empty( $categories ) ){
            $categories = array_keys( $categories );
            $catStr = implode( "\n", $categories );
            file_put_contents( 'categories', $catStr, FILE_APPEND | LOCK_EX );
        }
    }

    private function _friendsToCSV( $row){
        $values = '';
        if( !empty( $row ) ){
            $nodeData = json_decode( $row, true );
            if( is_array( $nodeData ) ){
                $uid1 = $nodeData['id'];
                if( isset( $uid1 ) ){
                    if( !empty( $nodeData['links'] ) ){
                        $links = $nodeData['links'];

                        foreach( $links as $linkId ){
                            if( $uid1 < $linkId ){
                                $values .= "$uid1,$linkId\n";
                            }
                            else{
                                $values .= "$linkId,$uid1\n";
                            }
                        }
                    }
                }
            }
        }
        return $values;
    }

    private static function _readData( $file ){
        $string = explode( " ", file_get_contents( $file ) );
        return $string;
    }

    private function _init(){
        $this->mysqli = new mysqli( self::$dbHost, 'root', '', 'initdb' );
    }

    public function __construct( ){
        $this->_init();
    }

} 