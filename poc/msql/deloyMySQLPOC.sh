# wget install
yum install -y wget

# JDK install
wget -O jdk-7u45-linux-x64.rpm --no-cookies --no-check-certificate --header "Cookie: gpw_e24=http%3A%2F%2Fwww.oracle.com%2Ftechnetwork%2Fjava%2Fjavase%2Fdownloads%2Fjdk7-downloads-1880260.html" "http://download.oracle.com/otn-pub/java/jdk/7u45-b18/jdk-7u45-linux-x64.rpm"
rpm -Uvh jdk-7u45-linux-x64.rpm

# Download test data generator
git clone https://eitan101@bitbucket.org/eitan101/initdb.git
cd initdb
./gradlew

# Install MySQL
yum install -y mysql-server

# Install php
yum install -y php
yum install -y php-pdo php-mysqli

# Tune MySQL configuration
cd .. && cp MySQLConf/my.cnf /etc/my.cnf

# Start MySQL
service mysqld start

# Grant remote access for root
echo "GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' IDENTIFIED BY '' WITH GRANT OPTION;" | mysql -u root

service mysqld restart

# Create database
echo "create database initdb" | mysql -u root

# create tables
mysql -u root initdb < MySQLConf/tables.sql

# Upload data
cd clients && php runImportMySQL.php
printf "\nImport friends from csv to MySQL\n"
mysqlimport --fields-terminated-by=, --local -u root initdb  friends.csv
printf "MySQL data preparing is done\n"

# Update categories
git add categories
git commit -m "Categories update"
git push

