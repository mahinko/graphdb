<?php
/**
 *  https://docs.google.com/document/d/1LEsPyzPnuZ-Rktropo3pL3HiStAZDQMo9AjmntN6uJU/edit#
 *
 * @version 1.0
 * Date: 12/24/13
 * Time: 2:29 PM
 */

require_once('../lib/neo4jphp.phar');

use Everyman\Neo4j\Client,
    Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Index\RelationshipIndex,
    Everyman\Neo4j\Batch,
    Everyman\Neo4j\Relationship,
    Everyman\Neo4j\Node,
    Everyman\Neo4j\Traversal,
    Everyman\Neo4j\Cypher;

date_default_timezone_set('UTC');

ini_set('memory_limit','2000M');

class Neo4jBridge {

    private $neo4jHost = null;
    public $neo4jConn;

    public function query( $startNodeId, $cat1, $cat2, $cat3 ){

        $result = null;
        $errorMessage = null;
        $errorCode = null;

        if( !empty( $cat1 ) && !empty( $cat2 ) && !empty( $cat3 ) ){
            $catIndex = 'Cat1Cat2Cat3';
            $category = "$cat1.$cat2.$cat3";
        }

        if( !empty( $cat1 ) && !empty( $cat2 ) && empty( $cat3 ) ){
            $catIndex = 'Cat1Cat2';
            $category = "$cat1.$cat2";
        }

        if( !empty( $cat1 ) && empty( $cat2 ) && empty( $cat3 ) ){
            $catIndex = 'Cat1';
            $category = $cat1;
        }

        $paths = array();
        $pathCounter = array();
        if( !empty( $cat1 ) && !empty( $catIndex ) && !empty( $startNodeId ) ){
            // Short paths processing
            try {
                $queryString = "START nodeA=node:Nodes('id:$startNodeId'),
                                item=node:Item$catIndex('category:\"$category\"')
                                MATCH nodeA-[:FRIEND]-friend2-[:DATA]->item
                                RETURN friend2.id, item";

                $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
                $results = $query->getResultSet();

                if( !empty( $results ) ) {
                    foreach( $results as $row ){
                        if( !isset( $paths[$row['friend2.id']] ) ){
                                $paths[$row['friend2.id']] = array(
                                $startNodeId,
                                null,
                                $row['friend2.id'],
                                array( $row['item']->getId() => $row['item']->getProperties() ),
                                1
                            );
                        }
                        else{
                            $pathCounter[$row['friend2.id']][null] = 1;
                            $paths[$row['friend2.id']][3][$row['item']->getId()] = $row['item']->getProperties();
                        }
                    }
                }
                $result = 'success';
            } catch ( Exception $e ) {
                $errorMessage = $e->getMessage();
                $errorCode = $e->getCode();
            }

            // Long paths processing
            try {
                $queryString = "START nodeA=node:Nodes('id:$startNodeId'),
                                item=node:Item$catIndex('category:\"$category\"')
                                MATCH nodeA-[:FRIEND]-friend1-[:FRIEND]-friend2-[:DATA]->item
                                RETURN friend1.id, friend2.id, item";

                $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
                $results = $query->getResultSet();

                if( !empty( $results ) ) {

                    foreach( $results as $row ){
                        if( !isset( $paths[$row['friend2.id']] ) ){
                            $paths[$row['friend2.id']] = array(
                                $startNodeId,
                                $row['friend1.id'],
                                $row['friend2.id'],
                                array( $row['item']->getId() => $row['item']->getProperties() ),
                                1
                            );
                        }
                        else{
                            $pathCounter[$row['friend2.id']][$row['friend1.id']] = 1;
                            $paths[$row['friend2.id']][3][$row['item']->getId()] = $row['item']->getProperties();
                            $paths[$row['friend2.id']][4] = count( $pathCounter[$row['friend2.id']] );
                        }
                    }
                }
                $result = 'success';
            } catch ( Exception $e ) {
                $errorMessage = $e->getMessage();
                $errorCode = $e->getCode();
            }

        }
        else{
            $result = 'failure';
            $errorMessage = "Error: first category or cat index or start node id is not defined:$cat1";
            $errorCode = 2;
        }
        if( !empty( $errorMessage ) ){
            $result = 'failure';
        }

        return json_encode( array( 'paths' => array_values( $paths ), 'result' => $result, 'errorMessage' => $errorMessage, 'errorCode' => $errorCode  ) );
    }

    public function getDateOfLastUpdate( $nodeId ){
        $result = null;
        $errorMessage = null;
        $errorCode = null;
        $dateOfLastUpdate = 0;

        if( !empty( $nodeId ) ){
            try {
                $queryString = "START n=node:Nodes('id:$nodeId')
                                RETURN n.timeOfLastRelationshipsUpdate? as Date
                                LIMIT 1
                                ";

                $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
                $results = $query->getResultSet();

                if( !empty( $results ) ) {
                    foreach( $results as $row ){
                        if( !empty( $row['Date'] ) ){
                            $dateOfLastUpdate = $row['Date'];
                        }
                        else{
                            $dateOfLastUpdate = 0;
                        }
                    }
                }
                $result = 'success';
            } catch ( Exception $e ) {
                $errorMessage = $e->getMessage();
                $errorCode = $e->getCode();
            }
        }
        else{
            $result = 'failure';
            $errorMessage = "Error: Node Id is not defined";
            $errorCode = 4;
        }
        return json_encode( array( 'dateOfLastUpdate' => $dateOfLastUpdate, 'result' => $result, 'errorMessage' => $errorMessage, 'errorCode' => $errorCode  ) );
    }
    
    public function queryItems( $nodeId, $cat1, $cat2, $cat3 ){
        $itemsData = array();
        $result = null;
        $errorMessage = null;
        $errorCode = null;

        if( !empty( $nodeId ) ){
            $nodeFilter = "ownerNode=node:Nodes(id='$nodeId')";
        }
        else{
            $nodeFilter = '';
        }

        if( !empty( $cat1 ) && !empty( $cat2 ) && !empty( $cat3 ) ){
            $catIndex = 'Cat1Cat2Cat3';
            $category = "$cat1.$cat2.$cat3";
        }

        if( !empty( $cat1 ) && !empty( $cat2 ) && empty( $cat3 ) ){
            $catIndex = 'Cat1Cat2';
            $category = "$cat1.$cat2";
        }

        if( !empty( $cat1 ) && empty( $cat2 ) && empty( $cat3 ) ){
            $catIndex = 'Cat1';
            $category = $cat1;
        }

        if( !empty( $catIndex ) ){
            $categoryFilter = "item=node:Item$catIndex(category='$category')";
        }
        else{
            $categoryFilter = '';
        }

        if(!empty( $nodeFilter ) && !empty( $categoryFilter ) ){
            $delimiter = ',';
        }
        else{
            $delimiter = '';
        }

        if( !empty( $nodeFilter ) || !empty( $categoryFilter ) ){
            $queryString = "START
                $nodeFilter
                $delimiter
                $categoryFilter
                MATCH ownerNode-[:DATA]->item
                RETURN DISTINCT(item),ownerNode.id";

                try {
                    $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
                    $res = $query->getResultSet();
                    if( !empty( $res ) ) {
                        foreach ( $res as $row ) {
                            $itemsData[] = array(
                                $row[ 'ownerNode.id' ],
                                $row[ 'item' ]->getProperties()
                            );
                        }
                    }
                    $result = 'success';
                } catch ( Exception $e ) {
                    $result = 'failure';
                    $errorMessage = $e->getMessage();
                    $errorCode = $e->getCode();
                }
        }
        else{
            $errorMessage = "Error: node id and categories are not defined";
            $errorCode = 3;
            $result = 'failure';
        }
        return json_encode( array( 'itemsData' => $itemsData, 'result' => $result, 'errorMessage' => $errorMessage, 'errorCode' => $errorCode  ) );

    }
    
    public function getItemById( $itemId ){
        $itemData = array();
        $result = null;
        $errorMessage = null;
        $errorCode = null;
        if( !empty( $itemId ) ){
            $queryString = "START item=node:Items(id='$itemId')
                            MATCH ownerNode-[:DATA]->item
                            RETURN DISTINCT(item), ownerNode.id
                            LIMIT 1
                            ";
            try {
                $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
                $res = $query->getResultSet();
                if( !empty( $res ) ) {
                    foreach ( $res as $row ) {
                        $itemData[] = array(
                            $row[ 'ownerNode.id' ],
                            $row[ 'item' ]->getProperties()
                        );
                    }
                }
                $result = 'success';
            } catch ( Exception $e ) {
                $result = 'failure';
                $errorMessage = $e->getMessage();
                $errorCode = $e->getCode();
            }
        }
        return json_encode( array( 'itemData' => $itemData, 'result' => $result, 'errorMessage' => $errorMessage, 'errorCode' => $errorCode  ) );
    }

    public function deleteItemById( $itemId ){
        $itemData = array();
        $result = null;
        $errorMessage = null;
        $errorCode = null;
        if( !empty( $itemId ) ){

            $queryString = "START item=node:Items(id='$itemId')
                            MATCH item-[r]-()
                            DELETE r
                            RETURN item, item.category
                            ";

            try {
                $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
                $res = $query->getResultSet();
                if( !empty( $res ) ) {
                    $itemIndex = new Everyman\Neo4j\Index\NodeIndex( $this->neo4jConn, 'Items' );
                    $cat1Index = new Everyman\Neo4j\Index\NodeIndex( $this->neo4jConn, 'ItemCat1' );
                    $cat2Index = new Everyman\Neo4j\Index\NodeIndex( $this->neo4jConn, 'ItemCat1Cat2' );
                    $cat3Index = new Everyman\Neo4j\Index\NodeIndex( $this->neo4jConn, 'ItemCat1Cat2Cat3' );
                    foreach ( $res as $row ) {
                        $categoryData = explode( ".", $row[ 'item.category' ] );
                        $itemIndex->remove( $row[ 'item' ], 'id', $itemId );
                        $cat1Index->remove( $row[ 'item' ], 'category', $categoryData[0] );
                        $cat2Index->remove( $row[ 'item' ], 'category', "$categoryData[0].$categoryData[1]" );
                        $cat3Index->remove( $row[ 'item' ], 'category', $row[ 'item.category' ] );
                        $row[ 'item' ]->delete();
                    }
                }
                $result = 'success';
            } catch ( Exception $e ) {
                $result = 'failure';
                $errorMessage = $e->getMessage();
                $errorCode = $e->getCode();
            }

            try {

                if( !empty( $itemNode ) ){

                    $itemNode->delete();
                }
            } catch ( Exception $e ) {
                $result = 'failure';
                $errorMessage = $e->getMessage();
                $errorCode = $e->getCode();
            }
        }
        return json_encode( array( 'itemData' => $itemData, 'result' => $result, 'errorMessage' => $errorMessage, 'errorCode' => $errorCode  ) );
    }

    /**
     * Make data node related to the owner node. If data node with given id exists already then node is updated.
     * @param $nodeId
     * @param $data
     * @return string
     */
    public function makeItem( $nodeId, $data ){
        $result = null;
        $errorMessage = null;
        $errorCode = null;
        $itemId = null;
        $nodeIndex = new Everyman\Neo4j\Index\NodeIndex( $this->neo4jConn, 'Nodes' );
        try {
            $ownerNode = $nodeIndex->findOne( 'id', $nodeId );
        } catch ( Exception $e ) {
            echo $e->getMessage();
        }

        if( !empty( $ownerNode ) && !empty( $data ) && isset( $data['category'] ) && isset( $data['url'] ) ){
            try {
                /*
                $itemNode = $this->neo4jConn->makeNode();
                foreach ( $data as $propertyName => $propertyValue ) {
                    if( in_array( $propertyName, array( 'id', 'link_id', 'reference' ) ) ){
                        continue;
                    }
                    if( ( $propertyValue == 'NULL' ) || empty( $propertyValue ) ){
                        continue;
                    }
                    $itemNode->setProperty( $propertyName, $propertyValue );
                }
                $itemNode->save();
                $itemId = $itemNode->getId();
                */
                foreach ( $data as $propertyName => $propertyValue ) {
                    if( in_array( $propertyName, array( 'id', 'link_id', 'reference' ) ) ){
                        continue;
                    }
                    if( ( $propertyValue == 'NULL' ) || empty( $propertyValue ) ){
                        continue;
                    }
                    if( empty( $map ) ){
                        $map = "$propertyName: {".$propertyName."}";
                        $varMap[$propertyName] = $propertyValue;
                    }
                    else{
                        $map .= ", $propertyName: {".$propertyName."}";
                        $varMap[$propertyName] = $propertyValue;
                    }
                }

                if( !empty( $map ) ){
                    $map = "{".$map."}";
                    $queryString = "
                        START owner=node:Nodes('id:$nodeId')
                        WITH owner
                        CREATE owner-[:DATA]->( item $map )
                        RETURN item
                    ";
                    $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString, $varMap );
                    $result = $query->getResultSet();
                    if( !empty( $result ) ){
                        foreach( $result as $row ){
                            $itemNode = $row['item'];
                            $itemId = $itemNode->getId();
                        }
                    }
                }

                //$ownerNode->relateTo( $itemNode, 'DATA' )->save();

                if( !empty( $itemNode ) ){
                    $this->_addDataOwnerToCategoryIndex( $ownerNode, $data['category'] );
                    $this->_addItemToIndexes( $itemNode, $data, $itemId );
                    $result = 'success';
                }
            } catch ( Exception $e ) {
                $errorMessage = $e->getMessage();
                $errorCode = $e->getCode();
                $result = 'failure';
            }
        }
        else{
            $errorCode = 1;
            $errorMessage = 'Error:Owner node does not exist or url is not defined or category is not defined';
            $result = 'failure';
        }
        return json_encode( array( 'itemId' => $itemId, 'result' => $result, 'errorMessage' => $errorMessage, 'errorCode' => $errorCode  ) );
    }

    private function _addItemToIndexes( $itemNode, $data, $itemId ){
        $itemIndex = new Everyman\Neo4j\Index\NodeIndex( $this->neo4jConn, 'Items' );
        $itemIndex->add( $itemNode, 'id', $itemId );

        $categoryData = explode( '.', $data['category'] );
        if( !empty( $categoryData )  ){
            if( isset( $categoryData[0] ) ){
                $catIndex = new Everyman\Neo4j\Index\NodeIndex( $this->neo4jConn, 'ItemCat1' );
                $catIndex->add( $itemNode, 'category', $categoryData[0] );
            }
            if( isset( $categoryData[1] ) ){
                $catIndex = new Everyman\Neo4j\Index\NodeIndex( $this->neo4jConn, 'ItemCat1Cat2' );
                $catIndex->add( $itemNode, 'category', $categoryData[0].'.'.$categoryData[1] );
            }
            if( isset( $categoryData[2] ) ){
                $catIndex = new Everyman\Neo4j\Index\NodeIndex( $this->neo4jConn, 'ItemCat1Cat2Cat3' );
                $catIndex->add( $itemNode, 'category', $data['category'] );
            }
        }
    }

    private function _addDataOwnerToCategoryIndex( $ownerNode, $category ){
        $categoryData = explode( '.', $category );
        if( !empty( $categoryData )  ){
            if( isset( $categoryData[0] ) ){
                $cat1Index = new Everyman\Neo4j\Index\NodeIndex( $this->neo4jConn, 'Cat1' );
                $cat1Index->add( $ownerNode, 'category', $categoryData[0] );
            }
            if( isset( $categoryData[1] ) ){
                $cat1Index = new Everyman\Neo4j\Index\NodeIndex( $this->neo4jConn, 'Cat1Cat2' );
                $cat1Index->add( $ownerNode, 'category', $categoryData[0].'.'.$categoryData[1] );
            }
            if( isset( $categoryData[2] ) ){
                $cat1Index = new Everyman\Neo4j\Index\NodeIndex( $this->neo4jConn, 'Cat1Cat2Cat3' );
                $cat1Index->add( $ownerNode, 'category', $category );
            }
        }
    }

    /**
     * Add friends to the given node by a list. If friend does not exist any more then relationship is removed. Then if such old friend does not have any friend then it is removed as well.
     * @param       $nodeId
     * @param array $friends
     * @return string
     */
    public function updateFriends( $nodeId, $friends = array() ){

        $errorMessage = null;
        $errorCode = null;

        try {
            $nodeIndex = new Everyman\Neo4j\Index\NodeIndex( $this->neo4jConn, 'Nodes' );
            $friendIndex = new Everyman\Neo4j\Index\RelationshipIndex( $this->neo4jConn, 'Friends' );

            try {
                $startNode = $nodeIndex->findOne( 'id', $nodeId );
            } catch ( Exception $e ) {
                echo "Nodes index is not detected. Perhaps index does not exist. Create a new one.\n";
                $nodeIndex->save();
            }

            if( !empty( $startNode ) ) {
                $startNodeIdGrey = $startNode->getId();
                $startNode->setProperty( 'timeOfLastRelationshipsUpdate', strtotime( date( "Y-m-d" ) ) )->save();
                $oldFriends = $this->_getFriends( $startNodeIdGrey );
            } else {
                $startNode = $this->neo4jConn->makeNode()
                    ->setProperty( 'timeOfLastRelationshipsUpdate', strtotime( date( "Y-m-d" ) ) )
                    ->setProperty( 'id', (int)$nodeId )
                    ->save();
                $nodeIndex->add( $startNode, 'id', (int)$nodeId );
                $oldFriends = array();
            }

            if( !empty( $friends ) ) {
                $friendsState = $this->_filterFriendsByExistance( $friends, $nodeIndex );
                $nodesToFriend = $this->_filterExistingNodesToBeFriendLucene( $nodeId, $friendsState['exists'], $friendIndex );
                $this->_addFriendship( $startNode, $nodeId, $friendsState['notExists'], $nodesToFriend,  $nodeIndex, $friendIndex );

                if( !empty( $oldFriends ) ){
                    foreach ( $friends as $friendId ) {
                        // $oldFriends will contain friends which aren't a friend any more
                        if( isset( $oldFriends[ $friendId ] ) ) {
                            unset( $oldFriends[ $friendId ] );
                            continue;
                        }
                    }
                }

            }
            else{
                echo "No friends\n";
            }

            if( !empty( $oldFriends ) ){
                $this->_removeOldFriends( $nodeId, $oldFriends, $nodeIndex, $friendIndex );
            }

            $result = 'success';

        } catch ( Exception $e ) {
            $errorMessage = $e->getMessage();
            $errorCode = $e->getCode();
            $result = 'failure';
        }

        return json_encode( array( 'result' => $result, 'errorMessage' => $errorMessage, 'errorCode' => $errorCode  ) );
    }

    private function _filterExistingNodesToBeFriendLucene( $startNodeId, $friends, $friendIndex ){

        $existFriendNodes = array();
        $queries = static::_buildLuceneQueries( $startNodeId, $friends );

        if( !empty( $queries ) ){
            foreach( $queries as $query ){
                $friendRels = $friendIndex->query( $query );
                if( !empty( $friendRels ) ){
                    foreach( $friendRels as $rel ){
                        $existFriendNodes[$rel->getProperty( 'sId' ) ] = 1;
                        $existFriendNodes[$rel->getProperty( 'eId' ) ] = 1;
                    }
                }
            }
        }

        // Rel of friends with
        unset( $existFriendNodes[$startNodeId] );
        if( !empty( $existFriendNodes ) ){
            foreach( array_unique( array_keys( $existFriendNodes ) ) as $nodeId ){
                unset( $friends[$nodeId] );
            }
        }

        return $friends;
    }

    private static function _buildLuceneQueries( $startNodeId, $friends ){
        $query = '';
        $queries = array();
        foreach( $friends as $friendId => $friendNode ){
            if( $startNodeId < $friendId ){
                $ids = "$startNodeId;$friendId";
            }
            else{
                $ids = "$friendId;$startNodeId";
            }

            if( strlen( $query ) < 2000 ){
                $query .= "ids:$ids OR ";
            }
            else{
                $queries[] = $query;
                $query = '';
            }
        }

        if( !empty( $query ) ){
            $queries[] = $query;
        }

        if( !empty( $queries ) ){
            for( $i=0; $i< count( $queries  ); $i++ ){
                $queries[$i] = substr($queries[$i], 0, -3);
            }
        }
        return $queries;
    }

    private function _addFriendship( $startNode, $startNodeId, $friendsNotExisted, $nodesToFriend,  $nodeIndex, $friendIndex ){
        if( !empty( $friendsNotExisted ) || !empty( $nodesToFriend ) ){
            $this->neo4jConn->startBatch();
            // Make not existed node, make friendship
            foreach( $friendsNotExisted as $friendId ){
                $friendNode = $this->neo4jConn->makeNode()
                    ->setProperty( 'id', (int)$friendId )
                    ->save();
                $nodeIndex->add( $friendNode, 'id', (int)$friendId );
                $rel = $startNode->relateTo( $friendNode, 'FRIEND' )
                    ->setProperty( 'sId', (int)$startNodeId )
                    ->setProperty( 'eId', (int)$friendId )
                    ->save();

                if( $startNodeId < $friendId ){
                    $friendIndex->add( $rel, 'ids', "$startNodeId;$friendId" );
                }
                else{
                    $friendIndex->add( $rel, 'ids', "$friendId;$startNodeId" );
                }
            }

            // Make friendship for existed nodes
            if( !empty( $nodesToFriend ) ){
                foreach( $nodesToFriend as $friendId => $friendNode ){
                    $rel = $startNode->relateTo( $friendNode, 'FRIEND' )
                        ->setProperty( 'sId', (int)$startNodeId )
                        ->setProperty( 'eId', (int)$friendId )
                        ->save();
                    if( $startNodeId < $friendId ){
                        $friendIndex->add( $rel, 'ids', "$startNodeId;$friendId" );
                    }
                    else{
                        $friendIndex->add( $rel, 'ids', "$friendId;$startNodeId" );
                    }
                }
            }
            $this->neo4jConn->commitBatch();
        }
    }

    private function _filterFriendsByExistance( $friends, $nodeIndex ){

        $friendsState['notExists'] = array();
        $friendsState['exists'] = array();

        $queryFirst = '';
        $querySecond = '';
        foreach( $friends as $friendId ){
            if( strlen( $queryFirst ) < 2000 ){
                $queryFirst .= "id:$friendId OR ";
            }
            else{
                $querySecond .= "id:$friendId OR ";
            }

        }

        if( !empty( $queryFirst ) ){
            $queryFirst = rtrim( $queryFirst );
            $queryFirst = rtrim( $queryFirst, "R" );
            $queryFirst = rtrim( $queryFirst, "O" );

            $friendNodes = $nodeIndex->query( $queryFirst );

            if( !empty( $friendNodes ) ){
                foreach( $friendNodes as $node ){
                    $friendsState['exists'][$node->getProperty('id')] = $node;
                }
            }
        }

        if( !empty( $querySecond ) ){
            $querySecond = rtrim( $querySecond );
            $querySecond = rtrim( $querySecond, "R" );
            $querySecond = rtrim( $querySecond, "O" );
            $friendNodes = $nodeIndex->query( $querySecond );

            if( !empty( $friendNodes ) ){
                foreach( $friendNodes as $node ){
                    $friendsState['exists'][$node->getProperty('id')] = $node;
                }
            }
        }

        $friendsState['notExists'] = array_diff( $friends, array_keys( $friendsState['exists'] ) );

        return $friendsState;
    }

    private function _removeOldFriends( $nodeId, $oldFriends, $nodeIndex, $friendIndex ){
        if( !empty( $oldFriends ) ){
            foreach( $oldFriends as $friendId => $friendIdGrey ){

                // Remove from index
                if( $nodeId < $friendId ){
                    $ids = "$nodeId;$friendId";
                }
                else{
                    $ids = "$friendId;$nodeId";
                }
                $rels = $friendIndex->query( "ids:$ids" );
                if( !empty( $rels ) ){
                    foreach( $rels as $rel ){
                        $friendIndex->remove( $rel, 'ids', $ids );
                        $rel->delete();
                    }
                }

                // Delete friendship
                $queryString = "START oldFriend=node($friendIdGrey) " .
                    "MATCH oldFriend-[m]-() ".
                    "RETURN COUNT(m) AS RelNum, TYPE(m) AS RelType";

                $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
                $result = $query->getResultSet();

                // Handle standalone nodes without friends
                if( !empty( $result ) ){
                    $isAlone = true;
                    $hasData = false;
                    foreach( $result as $row ){
                        if( ( $row['RelType'] == 'FRIEND' ) && !empty( $row['RelNum'] ) ){
                            $isAlone = false;
                        }
                        if( ( $row['RelType'] == 'DATA' ) && !empty( $row['RelNum'] ) ){
                            $hasData = true;
                        }
                    }
                    if( $isAlone && !$hasData ){
                        $oldFriendNode = $this->neo4jConn->getNode( $friendIdGrey );
                        $nodeIndex->remove( $oldFriendNode, 'id', $friendId );
                        $oldFriendNode->delete();
                    }
                }
            }
        }
    }

    private function _getFriends( $nodeId ){
        $friends = array();
        $queryString = "START nodeA=node($nodeId) " .
            "MATCH nodeA-[r:FRIEND]-friend ".
            "RETURN DISTINCT friend.id AS friendId, ID(friend) AS friendIdGrey";

        $query = new Everyman\Neo4j\Cypher\Query( $this->neo4jConn, $queryString );
        $result = $query->getResultSet();
        if( !empty( $result ) ){
            foreach( $result as $row ){
                $friends[ $row['friendId'] ] = $row['friendIdGrey'];
            }
        }
        return $friends;
    }

    private function _init(){
        $this->neo4jConn = new Client( $this->neo4jHost );
    }

    public function __construct( ){
        $this->_init();
    }

} 