<?php
/**
 *  
 *
 * @version 1.0
 * Date: 1/2/14
 * Time: 3:23 PM
 */

require_once('../lib/neo4jphp.phar');
require_once('Neo4jBridge.php');

use Everyman\Neo4j\Client,
    Everyman\Neo4j\Index\NodeIndex,
    Everyman\Neo4j\Batch,
    Everyman\Neo4j\Relationship,
    Everyman\Neo4j\Node,
    Everyman\Neo4j\Cypher;

date_default_timezone_set('UTC');

ini_set('memory_limit','16000M');

class Uploader {
    private $bridge;

    public function uploadItems( $CSVFile ){
            file_put_contents( 'errorRecords.log', '' );
            $CSVdata = explode( "\"\n\"", file_get_contents( $CSVFile ) );
            if( !empty( $CSVdata ) ){
                $count = count( $CSVdata );
                $counter = 0;
                $i = 0;
                foreach( $CSVdata as $row ){
                    if( !empty( $row ) ){
                        //if( stripos( $row, 'http://www.b144.co.il/B144_sip/members/401C04134774605F4A150411.aspx') == false ){
                        //    continue;
                        //}
                        $itemData = static::_getData( $row );
                        $respond = $this->bridge->makeItem( $itemData['ownerId'], $itemData['data'] );
                        $respond = json_decode( $respond, true );
                        if( !empty( $respond['errorMessage'] ) ){
                            echo $respond['errorMessage']."\n";
                            if( $respond['errorMessage'] != 'Error:Owner node does not exist or url is not defined or category is not defined' ){
                                file_put_contents( 'errorRecords.log', '"'.$row."\"\n", FILE_APPEND );
                            }
                        }
                        $counter++;
                        $i++;
                        if( $counter > 1000 ){
                            $progress = round( ($i/$count)*100, 2 );
                            echo "$progress%\n";
                            $counter = 0;
                        }
                    }
                }
            }
    }

    private static function _getData( $itemRow ){
        // Fix doc exploding. Sting can contain \n in between
        $itemRow = "\"$itemRow\"";
        $itemRow = str_replace( ',NULL,', ',"NULL",', $itemRow );
        $itemRow = str_replace( ',NULL,', ',"NULL",', $itemRow );
        $itemRow = str_replace( ',"",', ',"NULL",', $itemRow );
        $item = explode( '","', $itemRow );
        $data = array();
        $owner = (int)str_replace( '"', '', $item[2] );
        $owner_comment = str_replace( '"', '', $item[3] );
        if( !empty( $owner_comment ) ){
            $data['owner_comment'] = $owner_comment;
        }
        $created_time = (int)str_replace( '"', '', $item[4] );
        if( !empty( $created_time ) ){
            $data['created_time'] = $created_time;
        }
        $url = str_replace( '"', '', $item[5] );
        if( !empty( $url ) ){
            $data['url'] = $url;
        }
        $email = str_replace( '"', '', $item[6] );
        if( !empty( $email ) ){
            $data['email'] = $email;
        }
        $title = $item[7];
        if( !empty( $title ) ){
            $data['title'] = str_replace( '\\', '', $title );
        }
        $category = str_replace( '"', '', $item[8] );
        if( !empty( $category ) ){
            $data['category'] = "a.b.$category";
        }
        $img = str_replace( '"', '', $item[9] );
        if( !empty( $img ) ){
            $data['img'] = $img;
        }
        $rate = str_replace( '"', '', $item[10] );
        if( !empty( $rate ) ){
            $data['rate'] = $rate;
        }
        $city = rtrim( $item[12], '"' );
        if( !empty( $city ) && ( stripos( $city, 'null' ) == false ) ){
            $data['city'] = str_replace( '\\', '', $city );
        }

        return array( 'ownerId' => $owner, 'data' => $data );
    }

    public function uploadFriends( $friendsCSVFile ){
            $friendsData = static::_prepareFriendsData( $friendsCSVFile );
            if( !empty( $friendsData ) ){
                $count = count( $friendsData );
                $counter = 0;
                $i = 0;
                foreach( $friendsData as $friendA => $friends ){
                    $this->bridge->updateFriends( $friendA, array_unique( $friends ) );
                    $counter++;
                    $i++;
                    if( $counter > 1000 ){
                        $progress = round( ($i/$count)*100, 2 );
                        echo "$progress%\n";
                        $counter = 0;
                        unset( $bridge );
                        $bridge = new Neo4jBridge();
                    }
                }
            }
    }

    private static function _prepareFriendsData( $friendsCSVFile ){
        $friendsData = array();
        if( is_file( $friendsCSVFile ) ){
            $CSVdata = explode( "\r\n", file_get_contents( $friendsCSVFile ) );
            foreach( $CSVdata as $row ){
                if( !empty( $row ) ){
                    $friends = explode( ",", $row );
                    if( !empty( $friends ) ){
                        $friendA = (int)str_replace( '"', '', $friends[0] );
                        $friendB = (int)str_replace( '"', '', $friends[1] );
                        if( $friendA == $friendB ){
                            continue;
                        }
                        $friendsData[$friendA][] = $friendB;
                    }
                }
            }
            unset( $CSVdata );
        }
        return $friendsData;
    }

    public function __construct( ){
        $this->bridge = new Neo4jBridge();
        $nodeIndex = new Everyman\Neo4j\Index\NodeIndex( $this->bridge->neo4jConn, 'Nodes' );
        $nodeIndex->save();
        $friendIndex = new Everyman\Neo4j\Index\RelationshipIndex( $this->bridge->neo4jConn, 'Friends' );
        $friendIndex->save();
    }

} 